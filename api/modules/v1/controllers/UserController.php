<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Branch;
use api\modules\v1\models\Customer;
use api\modules\v1\models\Roles;
use api\modules\v1\models\Supplier;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\RateLimiter;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use api\helpers\CommonHelper;
use api\modules\v1\models\User;
use api\modules\v1\models\search\UserSearch;

use api\models\LoginForm;
use api\models\SignupForm;

class UserController extends ActiveController
{
    public $modelClass = User::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['options'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['rateLimiter'] = [
            'class' => RateLimiter::className(),
            'enableRateLimitHeaders' => false,
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'], // ['http://localhost:8100', 'http://www.myserver.com', 'https://www.myserver.com'], Accepted host
                'Access-Control-Request-Method' =>  ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page', 'X-Pagination-Page-Count', 'X-Pagination-Per-Page', 'X-Pagination-Total-Count', 'Link'],
            ],
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create'], $actions['update'], $actions['delete']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $searchModel = new UserSearch();
        $searchModelName = StringHelper::basename(get_class($searchModel));

        $params = Yii::$app->request->queryParams;
        $reservedParams = CommonHelper::$reservedParams;

        $search = [];
        if (!empty($params)) {
            foreach($params as $key => $value){
                if(!is_scalar($key) || !is_scalar($value)) {
                    throw new HttpException(400, 'Invalid attribute:' . $key . Json::encode($value));
                }
                else if ($searchModel->hasAttribute($key)) {
                    $search[$searchModelName][$key] = $value;
                }
                else if (in_array($key, $reservedParams)) {
                    $search[$key] = $value;
                }
            }
        }

        $dataProvider = $searchModel->search($search);

        if($dataProvider->getCount() <= 0) {
            throw new HttpException(404, 'No results found.');
        }
        else {
            return $dataProvider;
        }
    }

    /**
     * Login
     *
     * @param username, password
     */
    public function actionLogin()
    {
        $model = new LoginForm();

        $post = Yii::$app->request->post();
        $data['LoginForm'] = $post;

        if(isset($post['email']) && !empty($post['email'])){
           $customerInfo = Customer::getCustomerInfoByEmail($post['email']);
           if(empty($customerInfo)){
               throw new HttpException(422, 'Email address not valid');
           }

           $userInfo = User::findUserByCustomerId($customerInfo->id);
           if(empty($userInfo)){
               throw new HttpException(422, 'User is inactive');
           }

           // Set user name
            $data['LoginForm']['username'] = $userInfo->username;
        }

        if ($model->load($data) && $model->login()) {
            $user = $model->getUser();

            //$user->generateAccessTokenAfterUpdatingClientInfo(true);

            $response = Yii::$app->getResponse();
            $response->setStatusCode(200);

            return $user;
        }
        else {
            throw new HttpException(422, json_encode($model->errors));
        }
    }

    /**
     * Sign up
     *
     * @param string userType (3 = Customer, 4= Supplier) title, name, gender, addressline, city, phone, email, username, password
     * @return array with access_token for the new user or errors from validate()
     */
    public function actionSignup()
    {
        $post = Yii::$app->request->post();

        if (isset($post)) {
            $transaction = Yii::$app->db->beginTransaction();
            try {

                $data = [];
                $customerId = 0;
                $supplierId = 0;

                if(isset($post['userType']) && $post['userType'] == CommonHelper::USER_TYPE_CUSTOMER)
                {
                    // Create Customer
                    $modelCustomer = new Customer();

                    $data['Customer'] = $post;

                    $modelCustomer->load($data);
                    $modelCustomer->custId = 'CN' . date("Ymdhis");
                    $modelCustomer->custType = CommonHelper::CUSTOMER_TYPE_GENERAL;
                    $modelCustomer->status = CommonHelper::STATUS_ACTIVE;

                    if (!$modelCustomer->save()) {
                        throw new HttpException(422, json_encode($modelCustomer->errors));
                    }

                    $customerId = $modelCustomer->id;
                }
                else if(isset($post['userType']) && $post['userType'] == CommonHelper::USER_TYPE_SUPPLIER)
                {
                    // Create Supplier
                    $modelSupplier = new Supplier();

                    $data['Supplier'] = $post;

                    $modelSupplier->load($data);
                    $modelSupplier->suppId = 'SN' . date("ymdhis");
                    $modelSupplier->crType = CommonHelper::CREDIT_SUPP;
                    $modelSupplier->status = CommonHelper::STATUS_INACTIVE;

                    if (!$modelSupplier->save()) {
                        throw new HttpException(422, json_encode($modelSupplier->errors));
                    }

                    $supplierId = $modelSupplier->id;
                }
                else{
                    throw new HttpException(422, 'Missing userType attribute.');
                }

                // User
                $model = new SignupForm();

                $data['SignupForm'] = $post;
                $model->load($data);
                $model->branchId = Branch::getDefaultBranch(CommonHelper::STATUS_ACTIVE);
                $model->custId = $customerId;
                $model->supplierId = $supplierId;

                if (!$model->validate()) {
                    throw new HttpException(422, json_encode($model->errors));
                }
                else if (!$model->signup()) {
                    throw new HttpException(422, json_encode($model->errors));
                }

                $userId = $model->_user->id;
                $access_token = $model->_user->auth_key;
         
                
                //Successful Email Template 
               
                $modelCustomer->email;
                $modelCustomer->custId;
                
                $bodymsg .='
                <html>
                    <head>
                        <meta charset="utf-8">
                        <style>
                            .registrationsuccess{
                                max-width:600px;
                                margin: auto;
                                padding: 10px;
                                border:1px solid #ddd;
                            }
                            .registrationsuccess .head{
                                font-size: 26px;
                                text-align: center;
                                background: #f2af58;
                                padding: 14px;color: #fff;
                                margin: -30px -30px 15px;
                            }
                        </style>
                    </head><body>';
                
                $bodymsg .= '<div class="registrationsuccess">';
                    $bodymsg .= '<div class="head">Thank you for registering with <a style="color:#fff" href="http://www.doeel.com/">doeel.com</a> </div>';
                    $bodymsg .= '<div style="padding:20px 0"><h2>Hello, '. $modelCustomer->name.'</h2>';
                    $bodymsg .= '<p style="font-size: 16px;"><strong style="text-transform: uppercase;">Email: </strong>'.$modelCustomer->email.'</p>';
                    $bodymsg .= '<p style="font-size: 16px;"><strong style="text-transform: uppercase;">Username: </strong>'.$model->username.'</p>';
                    $bodymsg .= '<p style="font-size: 16px;"><strong style="text-transform: uppercase;">Password: </strong>'.$model->password.'</p></div>';
                $bodymsg .='<table style="width: 100%;text-align: center;border-top: 1px solid #ddd;padding-bottom: 20px;padding-top: 20px;">
                        <tbody>
                            <tr>
                                <td colspan="3"><a style="font-size:20px;text-transform:uppercase">Helpline Number: +8801777600012</a></td>
                            </tr>
                            <tr>
                                <td colspan="3"><h3 style="text-transform: uppercase;font-size:20px">Needs Help ???</h3></td>
                            </tr>
                            <tr>
                                <td width="32%"><a style="font-size:16px;text-transform: uppercase;" href="http://www.doeel.com/#/about-us">About Us</a></td>
                                <td width="32%"><a style="font-size:16px;text-transform: uppercase;" href="http://www.doeel.com/#/returns-policy">Returns Policy</a></td>
                                <td width="32%"><a style="font-size:16px;text-transform: uppercase;" href="http://www.doeel.com/#/service-center">Service Center</a></td>
                            </tr>
                        </tbody>
                    </table></div>
                </body>';
                
                
/*                echo '<pre>';
                //print_r($model);
                print_r($modelCustomer);
                //print_r(Yii::$app->getResponse());
                echo '</pre>';
                exit();*/
                
                
         
                 Yii::$app->mailer->compose()
                ->setFrom(['info@doeel.com' => $company->name.' Registration'])
                ->setTo([
                    $modelCustomer->email => $modelCustomer->name,
                    'info@doeel.com' => 'Simanto SA',
                ])
                ->setSubject('Welcome, '.$modelCustomer->name)
                ->setHtmlBody($bodymsg)
                ->send();
         
                // Roles
                $modelRoles = new Roles();
                $modelRoles->userTypeId = CommonHelper::USER_TYPE_CUSTOMER;
                $modelRoles->userId = $userId;
                $modelRoles->status = CommonHelper::STATUS_ACTIVE;

                if (!$modelRoles->save()) {
                    throw new HttpException(422, json_encode($modelRoles->errors));
                }

                $response = Yii::$app->getResponse();
                $response->setStatusCode(200);

                $transaction->commit();

                return ['access_token' => $access_token];
            }
            catch (Exception $e) {
                $transaction->rollBack();
                throw new ServerErrorHttpException($e->getMessage());
            }
        }
    }

    public function actionAccount()
    {
        $user = Yii::$app->user->identity;
      
        $user->setScenario("account");

        if ($user->load(Yii::$app->request->post()) && $user->validate())
        {
            if(isset($user->newPassword))
            {
                $salt = uniqid('', true);
                $user->password = $user->hashPassword($user->newPassword, $salt);
                $user->salt = $salt;
                /*$user->generateAuthKey();*/
            }

            if (!$user->save(false)) {
                throw new HttpException(422, json_encode($user->errors));
            }

            $response = Yii::$app->getResponse();
            $response->setStatusCode(200);

            return $user;
        }

        return $this->render('account', [
            'user' => $user,
        ]);
    }
    

}


