<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\web\HttpException;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\filters\Cors;
use yii\filters\RateLimiter;
use yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\models\User;

class PaysenzTokenController extends ActiveController
{
    public $modelClass = User::class;
    
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['options'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['rateLimiter'] = [
            'class' => RateLimiter::className(),
            'enableRateLimitHeaders' => false,
        ];

        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' =>  ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page', 'X-Pagination-Page-Count', 'X-Pagination-Per-Page', 'X-Pagination-Total-Count', 'Link'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create'], $actions['update'], $actions['delete']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     * Prepare access token for Paysenz
     *
     * HTTP Verb: GET
     * URL: /paysenz-token
     *
     * @return \yii\data\ActiveDataProvider
     * @throws HttpException
     */
    public function prepareDataProvider()
    {
        $access_token = $this->_retrieveToken();
        return array('access_token' => $access_token);
    }
    
    private function _retrieveToken(){
        try{
            $requestParams = [
                'grant_type' => 'password',
                'client_id' => Yii::$app->params['paysenz_client_id'],
                'client_secret' => Yii::$app->params['paysenz_client_secret'],
                'username' => Yii::$app->params['paysenz_client_username'],
                'password' => Yii::$app->params['paysenz_client_password'],
                'scope' => '*',
            ];

            $payload = json_encode($requestParams);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, Yii::$app->params['paysenz_tokenEndpoint']);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);

            // Set HTTP Header for POST request
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($payload))
            );

            $response = curl_exec($ch);
            curl_close($ch);

            $content = json_decode((string) $response, true);
            return  $content['access_token'];      
            
        }catch (Exception $e){
            return array('error' => true);
        }
    }
}