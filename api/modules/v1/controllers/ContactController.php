<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\base\Exception;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\filters\RateLimiter;
use yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\helpers\CommonHelper;
use api\modules\v1\models\Contact;

class ContactController extends ActiveController
{
    public $modelClass = Contact::class;
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['options'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['rateLimiter'] = [
            'class' => RateLimiter::className(),
            'enableRateLimitHeaders' => false,
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' =>  ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page', 'X-Pagination-Page-Count', 'X-Pagination-Per-Page', 'X-Pagination-Total-Count', 'Link'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['view'], $actions['create'], $actions['update'], $actions['delete']);

        return $actions;
    }

    /**
     * Create a new record
     *
     * HTTP Verb: POST
     * URL: /companies
     * 'Content-Type': 'multipart/form-data'
     *
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionAdmissionEmail()
    {
        $modelClass = $this->modelClass;
        $model = new $modelClass();

        if ($model->load(Yii::$app->getRequest()->getBodyParams())) {
            $bodydata = '<html>
                <head>
                    <meta charset="utf-8">
                    <style>
                        table {
                            font-family: arial, sans-serif;
                            border-collapse: collapse;
                            width: 500px;
                        }
                        
                        td, th {
                            border: 1px solid #dddddd;
                            text-align: left;
                            padding: 8px;
                        }
                    </style>
                </head>
                <body>
                    <h2>Admission Form</h2>
                    <table>
                      <tr>
                        <td><strong>Name:</strong></td>
                        <td>'.$model->name.'</td>
                      </tr>
                      <tr>
                        <td><strong>Email:</strong></td>
                        <td>'.$model->email.'</td>
                      </tr>
                        <tr>
                        <td><strong>Phone:</strong></td>
                        <td>'.$model->phone.'</td>
                      </tr>
                        <tr>
                        <td><strong>Contact Type:</strong></td>
                        <td>'.$model->contacttype.'</td>
                      </tr>
                        <tr>
                        <td><strong>Message:</strong></td>
                        <td>'.$model->message.'</td>
                      </tr>
                    </table>
                </body>
                
                </html>    
            ';
            Yii::$app->mailer->compose()
                ->setFrom(['info@doeel.com' => $model->name])
                ->setTo([
                    $model->email => $model->name,
                    'info@doeel.com' => 'Simanto SA',
                ])
                ->setSubject($model->name)
                ->setHtmlBody($bodydata)
                ->send();

            // Success
            Yii::$app->getResponse()->setStatusCode(200);
            return ['status' => 200, 'message' => 'Email Successfully Send.'];

            // Error
            throw new HttpException(400, 'Error');
        }
        else{
            throw new BadRequestHttpException('Unable to process data due to invalid data type. Expecting: array. Example data: Contact[name]=Field Value');
        }
    }


}
