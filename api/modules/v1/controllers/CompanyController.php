<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\base\Exception;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\filters\RateLimiter;
use yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\helpers\CommonHelper;
use api\modules\v1\models\Company;
use api\modules\v1\models\search\CompanySearch;

class CompanyController extends ActiveController
{
    public $modelClass = Company::class;

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['options'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['rateLimiter'] = [
            'class' => RateLimiter::className(),
            'enableRateLimitHeaders' => false,
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' =>  ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page', 'X-Pagination-Page-Count', 'X-Pagination-Per-Page', 'X-Pagination-Total-Count', 'Link'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['view'], $actions['create'], $actions['update'], $actions['delete']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     * List all records page by page
     *
     * HTTP Verb: GET
     * URL: /companies
     *
     * @return \yii\data\ActiveDataProvider
     * @throws HttpException
     */
    public function prepareDataProvider()
    {
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if($dataProvider->getCount() <= 0) {
            throw new HttpException(404, 'No results found.');
        }
        else {
            return $dataProvider;
        }
    }

    /**
     * Return the details of the record {id}
     *
     * HTTP Verb: GET
     * URL: /companies/{id}
     *
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->findModel($id);
    }

    /**
     * Create a new record
     *
     * HTTP Verb: POST
     * URL: /companies
     * 'Content-Type': 'multipart/form-data'
     *
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionCreate()
    {
        $modelClass = $this->modelClass;
        $model = new $modelClass();

        if ($model->load(Yii::$app->getRequest()->getBodyParams())) {
            return $this->saveData($model);
        }
        else{
            throw new BadRequestHttpException('Unable to process data due to invalid data type. Expecting: array. Example data: ModelName[fieldName]=Field Value');
        }
    }

    /**
     * Updates an existing record
     *
     * HTTP Verb: PATCH, PUT
     * URL: /companies/{id}
     * 'Content-Type': 'application/x-www-form-urlencoded'
     *
     * @param $id
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->getRequest()->getBodyParams()))
        {
            return $this->saveData($model);
        }
        else{
            throw new BadRequestHttpException('Unable to process data due to invalid data type. Expecting: array. Example data: ModelName[fieldName]=Field Value');
        }
    }


    /**
     * Deletes an existing record
     *
     * HTTP Verb: DELETE
     * URL: /companies/{id}
     *
     * @param $id
     * @return array
     * @throws ServerErrorHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        try {
            if (!$model->delete()) {
                throw new Exception('The record cannot be deleted.');
            }

            // Delete Image:
            // FileHelper::removeFile($model->image, 'sanction');

            Yii::$app->getResponse()->setStatusCode(200);
            return ['status' => 200, 'message' => 'The record have been successfully deleted.'];
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            throw new ServerErrorHttpException($message);
        }

    }

    /**
     * Save Data.
     *
     * @param $model
     * @return mixed
     * @throws ServerErrorHttpException
     */
    private function saveData($model){
        if (isset($model)) {
            //$transaction = Yii::$app->db->beginTransaction();
            try {
                $post = Yii::$app->request->post();

                if (!$model->save()) {
                    throw new Exception(json_encode($model->errors));
                }

                //$transaction->commit();
                return $model;
            }
            catch (Exception $e) {
                //$transaction->rollBack();
                throw new ServerErrorHttpException($e->getMessage());
            }
        }
    }

    /**
     * Finds the record based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $modelClass = $this->modelClass;
        if (($model = $modelClass::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException("No records found");
        }
    }



}
