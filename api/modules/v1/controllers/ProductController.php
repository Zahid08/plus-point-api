<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\rbac\Item;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\RateLimiter;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use api\helpers\CommonHelper;
use api\modules\v1\models\Items;
use api\modules\v1\models\search\ItemsSearch;

class ProductController extends ActiveController
{
    public $modelClass = Items::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['options'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['rateLimiter'] = [
            'class' => RateLimiter::className(),
            'enableRateLimitHeaders' => false,
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'], // ['http://localhost:8100', 'http://www.myserver.com', 'https://www.myserver.com'], Accepted host
                'Access-Control-Request-Method' =>  ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => false,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page', 'X-Pagination-Page-Count', 'X-Pagination-Per-Page', 'X-Pagination-Total-Count', 'Link'],
            ],
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $products = Items::find()->where(['status'=>1])->orderBy(['id'=>SORT_DESC])->all();
        $productsResponseData=$this->ResponseData($products);
        return $productsResponseData;

        /*$searchModel = new ItemsSearch();
        $searchModelName = StringHelper::basename(get_class($searchModel));

        $params = Yii::$app->request->queryParams;
        $reservedParams = CommonHelper::$reservedParams;

        $search = [];
        if (!empty($params)) {
            foreach($params as $key => $value){
                if(!is_scalar($key) || !is_scalar($value)) {
                    throw new HttpException(400, 'Invalid attribute:' . $key . Json::encode($value));
                }
                else if ($searchModel->hasAttribute($key)) {
                    $search[$searchModelName][$key] = $value;
                }
                else if (in_array($key, $reservedParams)) {
                    $search[$key] = $value;
                }
            }
        }

        $dataProvider = $searchModel->search($search);

        if($dataProvider->getCount() <= 0) {
            throw new HttpException(404, 'No results found.');
        }
        else {
            return $dataProvider;
        }*/
    }


    public function ResponseData($products){
        $dataList=[];
        foreach ($products as $productsItem){
            $itemCode=!empty($productsItem->itemCode)?$productsItem->itemCode:'';
            $barCode=!empty($productsItem->barCode)?$productsItem->barCode:'';
            $itemName=!empty($productsItem->itemName)?$productsItem->itemName:'';
            $itemName_bd=!empty($productsItem->itemName_bd)?$productsItem->itemName_bd:'';
            $slugName=!empty($productsItem->slugName)?$productsItem->slugName:'';
            $productPictures=$this->productImages($productsItem->itemImages);
            $pictures=$productPictures['pictures'];
            $imageSize=$productPictures['imageSize'];
            $price=!empty($productsItem->costPrice)?$productsItem->costPrice:'';
            $sellPrice=!empty($productsItem->sellPrice)?$productsItem->sellPrice:'';
            $discount=0;
            $description=!empty($productsItem->description)?$productsItem->description:'';
            $stock=100;
            $categoryData=$productsItem->cat;
            $categoryName=!empty($categoryData->name)?$categoryData->name:'';
            $brandData=$productsItem->brand;
            $brand=!empty($brandData->name)?$brandData->name:'';
            $brandId=!empty($brandData->id)?$brandData->id:'';
            $isFeatured=!empty($productsItem->isFeatured)?$productsItem->isFeatured:'';

            $createDate=$productsItem->crAt;
            if(date("Y-m-d", strtotime($createDate)) == date('Y-m-d')){
                $newStatus=true;
            }else{
                $newStatus=false;
            }

            $rating=$productsItem->itemRating;

            $colors=$productsItem->attributescolor;
            $size=$productsItem->attributessize;

            $dataList[]=[
                "id"=>$productsItem->id,
                "name"=>$itemName,
                "slugName"=>$slugName,
                "name_bd"=>$itemName_bd,
                "price"=>(float)$price,
                "salePrice"=>(float)$sellPrice,
                "discount"=>(float)$discount,
                "pictures"=>$pictures,
                "imageSize"=>$imageSize,
                "shortDetails"=>strip_tags($description),
                "description"=>strip_tags($description),
                "stock"=>$stock,
                "new"=>$newStatus,
                "sale"=>true,
                "category"=>$categoryName,
                "colors"=>$colors,
                "size"=>$size,
                "tags"=>[$brand],
                "brandId"=>$brandId,
                "rating"=>$rating,
                "itemCode"=>$itemCode,
                "isFeatured"=>$isFeatured,
                "barCode"=>$barCode
            ];
        }
        return $dataList;
    }

    public function productImages($images){
        $picuturesArray = [];
        foreach ($images as $imagesItem) {
            if($imagesItem->image) {
                $imagePath = $this->getCacheImageUrl($imagesItem->image, 'medium');
                $picuturesArray[]=$imagePath;
            }
        }

        $imageArray = [];
        $count = 0;
        foreach ($images as $imagesItem) {
            if($imagesItem->image){
                $imageSmallUrl = self::getCacheImageUrl($imagesItem->image, 'small');
                $imageMediumUrl = self::getCacheImageUrl($imagesItem->image, 'medium');
                $imageLargeUrl = self::getCacheImageUrl($imagesItem->image, 'large');

                $imageArray[$count]['small'] = $imageSmallUrl;
                $imageArray[$count]['medium'] = $imageMediumUrl;
                $imageArray[$count]['large'] = $imageLargeUrl;
            }
            $count++;
        }

        return ['pictures'=>$picuturesArray,'imageSize'=>$imageArray];
    }


    public static function getCacheImageUrl($fileName, $imageType)
    {
        $fileURL = 'http://pluspointretail.unlockretail.com/media/catalog/product/cache';
        $dirArray = explode('/', $fileName);
        $name = pathinfo($fileName, PATHINFO_FILENAME);
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);

        return  $fileURL . '/' . $dirArray[0] . '/' . $dirArray[1] .  '/' . $name . '-' . $imageType . '.' . $extension;
    }


}
