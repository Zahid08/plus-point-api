<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\base\Controller;
use yii\base\Exception;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\Response;
use yii\filters\RateLimiter;
use yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\helpers\CommonHelper;

use api\modules\v1\models\Company;
use api\modules\v1\models\Branch;
use api\modules\v1\models\SocialNetwork;
use api\modules\v1\models\SalesInvoice;
use api\modules\v1\models\SalesDetails;

use api\modules\v1\models\Customer;
use api\modules\v1\models\Items;

class CheckoutController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['options'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['rateLimiter'] = [
            'class' => RateLimiter::className(),
            'enableRateLimitHeaders' => false,
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' =>  ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page', 'X-Pagination-Page-Count', 'X-Pagination-Per-Page', 'X-Pagination-Total-Count', 'Link'],
            ],
        ];

        return $behaviors;
    }


    // checkout-payment-process
    public function actionCheckoutPaymentProcess()
    {
        $post = Yii::$app->request->post();

        $response = [];
        $response['payment_status'] = 1;
        $response['redirect'] = 'No';
        $response['redirect_url'] = '';

        Yii::$app->getResponse()->setStatusCode(200);
        return $response;
    }


    // checkout-success-email
    public function actionCheckoutSuccessEmail()
    {
        
        $post = Yii::$app->request->post();
        
        if($post['id']){
            
            $id = $post['id'];
        
       /************Company Information*************/
        $company = Company::find()->where(['id' => 1])->one();
        
        
        /************Order Information*************/
        $orderModel = SalesInvoice::find()->where(['id' => $id])->one();
        
        // Status
        if($orderModel->status == '3'){
            $orderstatus = 'Processing';
        }elseif($orderModel->status == '1'){
            $orderstatus = 'Confirm';
        }elseif($orderModel->status == '4'){
            $orderstatus = 'Cancel';
        }elseif($orderModel->status == '5'){
            $orderstatus = 'Return';
        }
        
        // Order Date
        $date= date_create($orderModel->orderDate);
        $orderDate = date_format($date,"d-m-Y");
        
        /************Sales Item Details Information*************/
        $salesDetails = SalesDetails::find()->where(['salesId' => $orderModel->id])->all();
        
        
        /************Customer Information*************/
        $customerInfo = Customer::find()->where(['id' => $orderModel->custId])->one();
        
        

        $totalVal = array();
        foreach($salesDetails as $salesDetail){
            $salesItems = Items::find()->where(['id' => $salesDetail->itemId])->one();


            $totalVal[] = $salesDetail->salesPrice * $salesDetail->qty;
        } 
        $totalSum = array_sum($totalVal);
        
        //echo $orderModel->invNo;
/*        echo "<pre>";
        print_r($orderModel);
        echo "</pre>";
        exit();*/

$bodydata = '<html>
<head>
    <meta charset="utf-8">
    
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 10px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.subtotal td{
        border: 1px solid #eee;
    }
    
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    .rtl {
        direction: rtl;
        font-family: Tahoma, "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
    <div style="font-size: 30px;text-align: center;background: #f2af58;padding: 14px;color: #fff;margin: -30px -30px 15px;">Thank you for your order</div>
        <table cellpadding="0" cellspacing="0">';
            $bodydata .= '<tr class="top">
                <td colspan="3">
                    <table>
                        <tr>
                            <td align="left" width="50%" class="title">
                                <img src="http://pluspointretail.unlockretail.com/'.$company->logo.'" >
                            </td>
                            
                            <td align="right" width="50%">
                                <strong>Invoice #: </strong>'.$orderModel->invNo.'<br>
                                <strong>Order Date: </strong>'.$orderDate.'<br>
                                <strong>Status: </strong>'.$orderstatus.'
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>';
            
            $bodydata .= '<tr class="information">
                <td colspan="3">
                    <table>
                        <tr>
                            <td align="left" width="50%">
                                <h4><strong>'.$company->name.'</strong></h4>
                                '.$company->detailsinfo.'<br>
                                '.$company->email.'<br>
                                '.$company->domain.'
                            </td>
                            
                            <td align="right" width="50%">
                               <h4><strong>'.$customerInfo->title.' '.$customerInfo->name.'</strong></h4>
                                '.$orderModel->shippingAddress.'<br>
                                '.$customerInfo->email.'<br>
                                '.$customerInfo->phone.'
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <table class="items-list">
                <tr class="heading">
                    <td>
                        Product Name
                    </td>
                    <td width="12%">
                        Price
                    </td>
                    <td width="10%">
                        Quantity 
                    </td>
                    <td width="12%">
                        Total
                    </td>
                </tr>';
                
                foreach($salesDetails as $salesDetail){
                    $salesItems = Items::find()->where(['id' => $salesDetail->itemId])->one();
                    $bodydata .= '<tr class="item">
                        <td>
                           '.$salesItems->itemName.'
                        </td>
                       <td>
                            &#2547; '.$salesDetail->salesPrice.' 
                        </td>
                        <td style="text-align:center">
                            '.$salesDetail->qty.' 
                        </td>
                        <td>
                            &#2547; '.number_format($salesDetail->qty * $salesDetail->salesPrice).' 
                        </td>
                    </tr>';
                } 
                $bodydata .= '<tr class="subtotal">
                    <td style="text-align: right;" colspan="3"><strong>Sub Total:</strong></td>
                    <td>&#2547; '.number_format($totalSum).'</td>
                </tr>';
                $bodydata .= '<tr class="subtotal">
                    <td style="text-align: right;" colspan="3"><strong>Delivery Charge:</strong></td>
                    <td>&#2547; '.$orderModel->totalShipping.'</td>
                </tr>';
                $bodydata .= '<tr class="subtotal">
                    <td style="text-align: right;" colspan="3"><strong>Discount:</strong></td>
                    <td>&#2547; '.$orderModel->totalDiscount.'</td>
                </tr>';
                $bodydata .= '<tr class="subtotal">
                    <td style="text-align: right;" colspan="3"><strong>Total Amount:</strong></td>
                    <td>&#2547; '.$orderModel->totalPrice.'</td>
                </tr>';
            $bodydata .= '</table>';
            
            
    $bodydata .= '</table>
    <p style="text-align:center;padding-top:20px;font-size:20px">Thank you for shopping from  <a target="_blank" href="'.$company->domain.'">'.$company->name.'</a> !</p>
    </div>
</body>
</html>';
    
    
        Yii::$app->mailer->compose()
    ->setFrom(['info@doeel.com' => $company->name])
    ->setTo([
                $customerInfo->email => $customerInfo->name,
                'info@doeel.com' => 'Simanto SA',
            ])
    ->setSubject($company->name.' '.'Invoice # '.$orderModel->invNo)
    ->setHtmlBody($bodydata)
    ->send();
        
            
        }
    
        

   
/*         $response = [];
        $response['email_send'] = 1;

        Yii::$app->getResponse()->setStatusCode(200);*/
        return 1;
        
        exit();
        
       
    }

}
