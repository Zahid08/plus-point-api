<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\RateLimiter;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use api\helpers\CommonHelper;
use api\modules\v1\models\Category;
use api\modules\v1\models\search\CategorySearch;

class ProductCategoryController extends ActiveController
{
    public $modelClass = Category::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['options'],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['rateLimiter'] = [
            'class' => RateLimiter::className(),
            'enableRateLimitHeaders' => false,
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'], // ['http://localhost:8100', 'http://www.myserver.com', 'https://www.myserver.com'], Accepted host
                'Access-Control-Request-Method' =>  ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page', 'X-Pagination-Page-Count', 'X-Pagination-Per-Page', 'X-Pagination-Total-Count', 'Link'],
            ],
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $searchModel = new CategorySearch();
        $searchModelName = StringHelper::basename(get_class($searchModel));

        $params = Yii::$app->request->queryParams;
        $reservedParams = CommonHelper::$reservedParams;

        $search = [];
        if (!empty($params)) {
            foreach($params as $key => $value){
                if(!is_scalar($key) || !is_scalar($value)) {
                    throw new HttpException(400, 'Invalid attribute:' . $key . Json::encode($value));
                }
                else if ($searchModel->hasAttribute($key)) {
                    $search[$searchModelName][$key] = $value;
                }
                else if (in_array($key, $reservedParams)) {
                    $search[$key] = $value;
                }
            }
        }

        $dataProvider = $searchModel->search($search);

        if($dataProvider->getCount() <= 0) {
            throw new HttpException(404, 'No results found.');
        }
        else {
            return $dataProvider;
        }
    }

}
