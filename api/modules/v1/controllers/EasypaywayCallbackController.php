<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

use api\modules\v1\models\SalesInvoice;

class EasypaywayCallbackController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionIndex()
    {
        echo 'Index';
        return ExitCode::OK;
    }
    
    public function actionSuccess()
    {
        $this->_processPaymentResponse();
        // Exit
        return ExitCode::OK;
    }
    
    public function actionFail()
    {
        $this->_processPaymentResponse();
        // Exit
        return ExitCode::OK;
    }
    
    public function actionCancel()
    {
        echo 'Cancel';
        return ExitCode::OK;
    }
    
    public function actionConfirm()
    {
        echo 'Confirm';
        return ExitCode::OK;
    }
    
    private function _processPaymentResponse() {
        // Process Data
        $post = Yii::$app->request->post();
        $origin = $_SERVER['HTTP_ORIGIN'] ? $_SERVER['HTTP_ORIGIN'] : null;
        if($this->_checkValidReffererOrigin($origin)) {
            $payStatus = $post['pay_status'] ? $post['pay_status'] : null;
            $amountPaid = $post['other_currency'] ? (float) $post['other_currency'] : null;
            $storeAmount = $post['store_amount'] ? (float) $post['store_amount'] : null;
            $epw_service_charge_bdt = $post['epw_service_charge_bdt'] ? (float) $post['epw_service_charge_bdt'] : 0;
            $epw_txnid = $post['epw_txnid'] ? $post['epw_txnid'] : null;
            $mer_txnid = $post['mer_txnid'] ? $post['mer_txnid'] : null;
            $pay_time = $post['pay_time'] ? $post['pay_time'] : null;
            $bank_txn = $post['bank_txn'] ? $post['bank_txn'] : null;
            $card_number = $post['card_number'] ? $post['card_number'] : null;
            $card_type = $post['card_type'] ? $post['card_type'] : null;
            
            $orderId = $post['opt_a'] ? (int) $post['opt_a'] : null;
            $paymentId = $post['opt_b'] ? (int) $post['opt_b'] : null;
            $redirectUrl = $post['opt_c'] ? 'https:'.$post['opt_c'] : null;
            
            $trackingId = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $epw_txnid;
            $PaymentSummery = "Payment TransactionID: {$epw_txnid}, MarchentId: {$mer_txnid}, Payment Time: {$pay_time}, Bank Transaction No: {$bank_txn}";
            
            if( ($orderId > 0) ) {
                $order = SalesInvoice::findOne($orderId);
                if($order && $order->id) {
                    $orderTotalPrice = $order->totalPrice;
                    $amountDiff = $orderTotalPrice - $amountPaid;
                    
                    // If payment amout is right, then update the Order
                    if(($payStatus == 'Successful') && ($amountDiff < 1) ){
                        $order->status = 1; // 1= Success, 3 = Processing, 4 = Cancel, 5 = Return
                        $redirectUrl .= '?paymentSuccess=1';
                    } else if($payStatus == 'Failed'){
                        $PaymentSummery = 'Failed:: ' . $PaymentSummery;
                        $redirectUrl .= '?paymentSuccess=0';
                    } else {
                        $redirectUrl .= '?paymentSuccess=cancel';
                    }
            
                    $modelData = array('SalesInvoice');
                    $modelData['SalesInvoice']['cardAppNo'] = $card_type;
                    $modelData['SalesInvoice']['cardNo'] = $card_number;
                    $modelData['SalesInvoice']['remarks'] = $trackingId;// Store the Payment Gateway Tracking number;
                    $modelData['SalesInvoice']['message'] = $PaymentSummery;// Store the payment information array.
                    
                    $order->load($modelData);
                    if($order->save()) {
                        /*echo "<pre>Success Order save: ";print_r($order->getAttributes());echo "</pre>";*/
                        Yii::$app->response->redirect($redirectUrl);
                    } else {
                        /*echo "<pre>Error: ";print_r($order->errors);echo "</pre>";*/
                        $redirectUrl .= '?paymentSuccess=errors';
                        Yii::$app->response->redirect($redirectUrl);
                    }
                }    
            }
            
        } else {
            echo "Error: invalid Refferer origin";
        }
    }
    
    private function _checkValidReffererOrigin($origin) {
        if($origin == 'https://easypayway.com') {
            return true;
        }
        
        return false;
    }
    
}
