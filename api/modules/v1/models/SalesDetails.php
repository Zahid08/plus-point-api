<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%sales_details}}".
 *
 * @property string $id
 * @property string $salesId
 * @property string $itemId
 * @property double $qty
 * @property string $salesDate
 * @property double $costPrice
 * @property double $salesPrice
 * @property double $discountPrice
 * @property double $vatPrice
 * @property double $shippingPrice
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * @property User $crBy0
 * @property Items $item
 * @property User $moBy0
 * @property SalesInvoice $sales
 */
class SalesDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sales_details}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['salesId', 'itemId', 'crBy', 'moBy', 'status'], 'integer'],
            [['qty', 'costPrice', 'salesPrice', 'discountPrice', 'vatPrice', 'shippingPrice'], 'number'],
            [['salesDate', 'attributes_color', 'attributes_size', 'crAt', 'moAt'], 'safe'],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['itemId'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['itemId' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
            [['salesId'], 'exist', 'skipOnError' => true, 'targetClass' => SalesInvoice::className(), 'targetAttribute' => ['salesId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'salesId' => Yii::t('app', 'Sales ID'),
            'itemId' => Yii::t('app', 'Item ID'),
            'qty' => Yii::t('app', 'Qty'),
            'salesDate' => Yii::t('app', 'Sales Date'),
            'costPrice' => Yii::t('app', 'Cost Price'),
            'salesPrice' => Yii::t('app', 'Sales Price'),
            'discountPrice' => Yii::t('app', 'Discount Price'),
            'vatPrice' => Yii::t('app', 'Vat Price'),
            'shippingPrice' => Yii::t('app', 'Shipping Price'),
            'attributes_color' => Yii::t('app', 'Color'),
            'attributes_size' => Yii::t('app', 'Size'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::className(), ['id' => 'itemId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSales()
    {
        return $this->hasOne(SalesInvoice::className(), ['id' => 'salesId']);
    }

    /** Get All Active SalesDetails Drop Down List*/
    public static function salesDetailsDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
                $this->salesDate = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }

    public function fields()
    {
        $fields = parent::fields();

        $additionalFields = [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];

        $result = ArrayHelper::merge($fields, $additionalFields);

        return $result;
    }

    public function extraFields()
    {
        return [
            'item' => function ($model) {
                return ArrayHelper::getValue($model, 'item');
            },
            'sales' => function ($model) {
                return ArrayHelper::getValue($model, 'sales');
            }
        ];
    }
}
