<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%cont_actions}}".
 *
 * @property string $id
 * @property string $contId
 * @property string $name
 * @property string $description
 * @property string $layout
 * @property string $status
 * @property string $crBy
 * @property string $crAt
 * @property string $moBy
 * @property string $moAt
 *
 * @property BranchUsertypeActions[] $branchUsertypeActions
 * @property Controllers $cont
 * @property User $crBy0
 * @property User $moBy0
 */
class ContActions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cont_actions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contId', 'layout', 'status', 'crBy', 'moBy'], 'integer'],
            [['description'], 'string'],
            [['crAt', 'moAt'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['contId'], 'exist', 'skipOnError' => true, 'targetClass' => Controllers::className(), 'targetAttribute' => ['contId' => 'id']],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'contId' => Yii::t('app', 'Cont ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'layout' => Yii::t('app', 'Layout'),
            'status' => Yii::t('app', 'Status'),
            'crBy' => Yii::t('app', 'Cr By'),
            'crAt' => Yii::t('app', 'Cr At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'moAt' => Yii::t('app', 'Mo At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranchUsertypeActions()
    {
        return $this->hasMany(BranchUsertypeActions::className(), ['contActionsId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCont()
    {
        return $this->hasOne(Controllers::className(), ['id' => 'contId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /** Get All Active ContActions Drop Down List*/
    public static function contActionsDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }
}
