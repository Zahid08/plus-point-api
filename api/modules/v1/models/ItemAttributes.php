<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%item_attributes}}".
 *
 * @property string $id
 * @property integer $attTypeId
 * @property string $attId
 * @property string $itemId
 *
 * @property Attributes $att
 * @property AttributesType $attType
 * @property Items $item
 */
class ItemAttributes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%item_attributes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attTypeId', 'attId', 'itemId'], 'integer'],
            [['attId'], 'exist', 'skipOnError' => true, 'targetClass' => Attributes::className(), 'targetAttribute' => ['attId' => 'id']],
            [['attTypeId'], 'exist', 'skipOnError' => true, 'targetClass' => AttributesType::className(), 'targetAttribute' => ['attTypeId' => 'id']],
            [['itemId'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['itemId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'attTypeId' => Yii::t('app', 'Att Type ID'),
            'attId' => Yii::t('app', 'Att ID'),
            'itemId' => Yii::t('app', 'Item ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtt()
    {
        return $this->hasOne(Attributes::className(), ['id' => 'attId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttType()
    {
        return $this->hasOne(AttributesType::className(), ['id' => 'attTypeId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::className(), ['id' => 'itemId']);
    }

    /** Get All Active ItemAttributes Drop Down List*/
    public static function itemAttributesDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    public static function getItemAttributes($itemId)
    {
        $model = self::find()
            ->where('itemId =:itemId', [':itemId' => $itemId])
            ->all();

        $data = [];
        if($model){
            $count = 0;
            foreach ($model as $item) {
                $attributeTypeInfo = AttributesType::getAttributesTypeInfo($item->attTypeId);
                $attributeInfo = Attributes::getAttributesInfo($item->attId);

                $data[$count]['name'] = $attributeTypeInfo->name;
                $data[$count]['value'] = $attributeInfo->name;
                $count++;
           }
        }

        return $data;
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }
}
