<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%branch}}".
 *
 * @property string $id
 * @property string $branchIdPrefix
 * @property string $name
 * @property string $addressline
 * @property string $city
 * @property string $postalcode
 * @property string $phone
 * @property string $fax
 * @property string $email
 * @property string $vatrn
 * @property string $status
 * @property integer $isDefault
 * @property integer $isInvoiceFull
 * @property integer $isDueInvoice
 * @property integer $isSpecialDiscount
 * @property integer $isInstantDiscount
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * @property BankDeposit[] $bankDeposits
 * @property BankWithdraw[] $bankWithdraws
 * @property User $crBy0
 * @property User $moBy0
 * @property CustomerDeposit[] $customerDeposits
 * @property DamageWastage[] $damageWastages
 * @property Finance[] $finances
 * @property InvSettings[] $invSettings
 * @property InvSummary[] $invSummaries
 * @property Inventory[] $inventories
 * @property ItemsPrice[] $itemsPrices
 * @property PoReturns[] $poReturns
 * @property Poffers[] $poffers
 * @property PurchaseOrder[] $purchaseOrders
 * @property SalesInvoice[] $salesInvoices
 * @property SalesReturn[] $salesReturns
 * @property SalesSpecial[] $salesSpecials
 * @property Stock[] $stocks
 * @property StockTransfer[] $stockTransfers
 * @property StockTransfer[] $stockTransfers0
 * @property SupplierDeposit[] $supplierDeposits
 * @property SupplierWidraw[] $supplierWidraws
 * @property TemporaryWorker[] $temporaryWorkers
 * @property TemporaryWorkerWidraw[] $temporaryWorkerWidraws
 * @property Tracer[] $tracers
 */
class Branch extends \yii\db\ActiveRecord
{
    const DEFAULT_BRANCH=1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%branch}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'isDefault', 'isInvoiceFull', 'isDueInvoice', 'isSpecialDiscount', 'isInstantDiscount', 'crBy', 'moBy'], 'integer'],
            [['crAt', 'moAt'], 'safe'],
            [['branchIdPrefix'], 'string', 'max' => 5],
            [['name', 'addressline'], 'string', 'max' => 255],
            [['city', 'postalcode', 'vatrn'], 'string', 'max' => 150],
            [['phone', 'fax', 'email'], 'string', 'max' => 155],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'branchIdPrefix' => Yii::t('app', 'Branch Id Prefix'),
            'name' => Yii::t('app', 'Name'),
            'addressline' => Yii::t('app', 'Addressline'),
            'city' => Yii::t('app', 'City'),
            'postalcode' => Yii::t('app', 'Postalcode'),
            'phone' => Yii::t('app', 'Phone'),
            'fax' => Yii::t('app', 'Fax'),
            'email' => Yii::t('app', 'Email'),
            'vatrn' => Yii::t('app', 'Vatrn'),
            'status' => Yii::t('app', 'Status'),
            'isDefault' => Yii::t('app', 'Is Default'),
            'isInvoiceFull' => Yii::t('app', 'Is Invoice Full'),
            'isDueInvoice' => Yii::t('app', 'Is Due Invoice'),
            'isSpecialDiscount' => Yii::t('app', 'Is Special Discount'),
            'isInstantDiscount' => Yii::t('app', 'Is Instant Discount'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankDeposits()
    {
        return $this->hasMany(BankDeposit::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankWithdraws()
    {
        return $this->hasMany(BankWithdraw::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerDeposits()
    {
        return $this->hasMany(CustomerDeposit::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDamageWastages()
    {
        return $this->hasMany(DamageWastage::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinances()
    {
        return $this->hasMany(Finance::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvSettings()
    {
        return $this->hasMany(InvSettings::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvSummaries()
    {
        return $this->hasMany(InvSummary::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventories()
    {
        return $this->hasMany(Inventory::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemsPrices()
    {
        return $this->hasMany(ItemsPrice::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoReturns()
    {
        return $this->hasMany(PoReturns::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoffers()
    {
        return $this->hasMany(Poffers::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrders()
    {
        return $this->hasMany(PurchaseOrder::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesInvoices()
    {
        return $this->hasMany(SalesInvoice::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesReturns()
    {
        return $this->hasMany(SalesReturn::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesSpecials()
    {
        return $this->hasMany(SalesSpecial::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStocks()
    {
        return $this->hasMany(Stock::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockTransfers()
    {
        return $this->hasMany(StockTransfer::className(), ['fromBranchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockTransfers0()
    {
        return $this->hasMany(StockTransfer::className(), ['toBranchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplierDeposits()
    {
        return $this->hasMany(SupplierDeposit::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplierWidraws()
    {
        return $this->hasMany(SupplierWidraw::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemporaryWorkers()
    {
        return $this->hasMany(TemporaryWorker::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemporaryWorkerWidraws()
    {
        return $this->hasMany(TemporaryWorkerWidraw::className(), ['branchId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTracers()
    {
        return $this->hasMany(Tracer::className(), ['branchId' => 'id']);
    }

    /** Get All Active Branch Drop Down List*/
    public static function branchDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    // get default branch
    public static function getDefaultBranch($isActive)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $isActive])
            ->andWhere('isDefault =:isDefault', [':isDefault' => self::DEFAULT_BRANCH])
            ->one();

        return isset($model->id) ? $model->id : self::DEFAULT_BRANCH;
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }
}
