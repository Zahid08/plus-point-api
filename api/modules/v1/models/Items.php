<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%items}}".
 *
 * @property string $id
 * @property string $itemCode
 * @property string $barCode
 * @property string $itemName
 * @property string $itemName_bd
 * @property double $costPrice
 * @property double $sellPrice
 * @property string $catId
 * @property string $brandId
 * @property string $supplierId
 * @property string $taxId
 * @property string $negativeStock
 * @property string $isWeighted
 * @property integer $caseCount
 * @property integer $isEcommerce
 * @property integer $isFeatured
 * @property integer $isSales
 * @property integer $isParent
 * @property string $specification
 * @property string $specification_bd
 * @property string $shipping_payment
 * @property string $shipping_payment_bd
 * @property string $description
 * @property string $description_bd
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * @property DamageWastage[] $damageWastages
 * @property Inventory[] $inventories
 * @property ItemAttributes[] $itemAttributes
 * @property ItemImages[] $itemImages
 * @property ItemParents[] $itemParents
 * @property ItemParents[] $itemParents0
 * @property ItemReview[] $itemReviews
 * @property Brand $brand
 * @property Category $cat
 * @property User $crBy0
 * @property User $moBy0
 * @property Supplier $supplier
 * @property Tax $tax
 * @property ItemsPrice[] $itemsPrices
 * @property PoDetails[] $poDetails
 * @property PoReturns[] $poReturns
 * @property Poffers[] $poffers
 * @property SalesDetails[] $salesDetails
 * @property SalesReturn[] $salesReturns
 * @property Stock[] $stocks
 * @property StockTransfer[] $stockTransfers
 * @property TemporaryWorker[] $temporaryWorkers
 * @property Wishlist[] $wishlists
 */
class Items extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%items}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['costPrice', 'sellPrice'], 'number'],
            [['catId', 'brandId', 'supplierId', 'taxId', 'caseCount', 'isEcommerce', 'isFeatured', 'isSales', 'isParent', 'crBy', 'moBy', 'status'], 'integer'],
            [['negativeStock', 'isWeighted', 'specification', 'specification_bd', 'shipping_payment', 'shipping_payment_bd', 'description', 'description_bd', 'attributes_color', 'attributes_size'], 'string'],
            [['caseCount', 'specification', 'shipping_payment', 'description'], 'required'],
            [['crAt', 'moAt', 'slugName'], 'safe'],
            [['itemCode', 'barCode'], 'string', 'max' => 150],
            [['itemName', 'itemName_bd'], 'string', 'max' => 200],
            [['brandId'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::className(), 'targetAttribute' => ['brandId' => 'id']],
            [['catId'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['catId' => 'id']],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
            [['supplierId'], 'exist', 'skipOnError' => true, 'targetClass' => Supplier::className(), 'targetAttribute' => ['supplierId' => 'id']],
            [['taxId'], 'exist', 'skipOnError' => true, 'targetClass' => Tax::className(), 'targetAttribute' => ['taxId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'itemCode' => Yii::t('app', 'Item Code'),
            'barCode' => Yii::t('app', 'Bar Code'),
            'itemName' => Yii::t('app', 'Item Name (বাংলা)'),
            'itemName_bd' => Yii::t('app', 'Item Name'),
            'costPrice' => Yii::t('app', 'Cost Price'),
            'sellPrice' => Yii::t('app', 'Sell Price'),
            'catId' => Yii::t('app', 'Cat ID'),
            'brandId' => Yii::t('app', 'Brand ID'),
            'supplierId' => Yii::t('app', 'Supplier ID'),
            'taxId' => Yii::t('app', 'Tax ID'),
            'negativeStock' => Yii::t('app', 'Negative Stock'),
            'isWeighted' => Yii::t('app', 'Is Weighted'),
            'caseCount' => Yii::t('app', 'Case Count'),
            'isEcommerce' => Yii::t('app', 'Is Ecommerce'),
            'isFeatured' => Yii::t('app', 'Is Featured'),
            'isSales' => Yii::t('app', 'Is Sales'),
            'isParent' => Yii::t('app', 'Is Parent'),
            'specification' => Yii::t('app', 'Specification'),
            'specification_bd' => Yii::t('app', 'Specification (বাংলা)'),
            'shipping_payment' => Yii::t('app', 'Shipping Payment'),
            'shipping_payment_bd' => Yii::t('app', 'Shipping Payment (বাংলা)'),
            'description' => Yii::t('app', 'Description'),
            'description_bd' => Yii::t('app', 'Description (বাংলা)'),
            'attributes_color' => 'Color',
            'attributes_size' => 'Size',
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDamageWastages()
    {
        return $this->hasMany(DamageWastage::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventories()
    {
        return $this->hasMany(Inventory::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemAttributes()
    {
        return $this->hasMany(ItemAttributes::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemImages()
    {
        return $this->hasMany(ItemImages::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemParents()
    {
        return $this->hasMany(ItemParents::className(), ['parentId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemParents0()
    {
        return $this->hasMany(ItemParents::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemReviews()
    {
        return $this->hasMany(ItemReview::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemRating()
    {
        $totalRating =  $this->hasMany(ItemReview::className(), ['itemId' => 'id'])->sum('rating');
        $countRow =  $this->hasMany(ItemReview::className(), ['itemId' => 'id'])->count();
        if($countRow > 0){
            return round($totalRating / $countRow);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brandId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCat()
    {
        return $this->hasOne(Category::className(), ['id' => 'catId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Supplier::className(), ['id' => 'supplierId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTax()
    {
        return $this->hasOne(Tax::className(), ['id' => 'taxId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemsPrices()
    {
        return $this->hasMany(ItemsPrice::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoDetails()
    {
        return $this->hasMany(PoDetails::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoReturns()
    {
        return $this->hasMany(PoReturns::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoffers()
    {
        return $this->hasMany(Poffers::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesDetails()
    {
        return $this->hasMany(SalesDetails::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesReturns()
    {
        return $this->hasMany(SalesReturn::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStocks()
    {
        return $this->hasMany(Stock::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockTransfers()
    {
        return $this->hasMany(StockTransfer::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemporaryWorkers()
    {
        return $this->hasMany(TemporaryWorker::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWishlists()
    {
        return $this->hasMany(Wishlist::className(), ['itemId' => 'id']);
    }

    
    
    public function getAttributescolor()
    {
        $colors = $this->attributes_color;
        $colorsExplode = explode(',',$colors);
        $colorArrray =array_map('trim',$colorsExplode);
        return $colorArrray;
      
    }
     public function getAttributessize()
    {
        $size = $this->attributes_size;
        $sizeExplode = explode(',',$size);
        $sizeArrray =array_map('trim',$sizeExplode);
        return (object)$sizeArrray;
      
    }

    /** Get All Active Items Drop Down List*/
    public static function itemsDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }

    public static function getCacheImageUrl($fileName, $imageType)
    {

        $fileURL = 'http://pluspointretail.unlockretail.com/media/catalog/product/cache';

        $dirArray = explode('/', $fileName);

        $name = pathinfo($fileName, PATHINFO_FILENAME);
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);

        return  $fileURL . '/' . $dirArray[0] . '/' . $dirArray[1] .  '/' . $name . '-' . $imageType . '.' . $extension;
    }

    public function fields()
    {
        return [
            'id',
            'itemCode',
            'barCode',
            'itemName',
            'itemName_bd',
            'slugName',
            'small_image' => function ($model) {
                $imageUrl = ArrayHelper::getValue($model, 'itemImages.0.image');
                if($imageUrl){
                    $imageUrl = self::getCacheImageUrl($imageUrl, 'small');
                }

                $imageInfo = [];
                $imageInfo['url'] = $imageUrl;
                $imageInfo['title'] = ArrayHelper::getValue($model, 'itemImages.0.alt_text');
                $imageInfo['width'] = 100;
                $imageInfo['height'] = 100;
                return $imageInfo;
            },
            'medium_image' => function ($model) {
                $imageUrl = ArrayHelper::getValue($model, 'itemImages.0.image');
                if($imageUrl){
                    $imageUrl = self::getCacheImageUrl($imageUrl, 'medium');
                }
                $imageInfo = [];
                $imageInfo['url'] = $imageUrl;
                $imageInfo['title'] = ArrayHelper::getValue($model, 'itemImages.0.alt_text');
                $imageInfo['width'] = 240;
                $imageInfo['height'] = 240;
                return $imageInfo;
            },
            'large_image' => function ($model) {
                $imageUrl = ArrayHelper::getValue($model, 'itemImages.0.image');
                if($imageUrl){
                    $imageUrl = self::getCacheImageUrl($imageUrl, 'large');
                }

                $imageInfo = [];
                $imageInfo['url'] = $imageUrl;
                $imageInfo['title'] = ArrayHelper::getValue($model, 'itemImages.0.alt_text');
                $imageInfo['width'] = 1100;
                $imageInfo['height'] = 1100;
                return $imageInfo;
            },
            'images' => function ($model) {
                $images = ArrayHelper::map($model->itemImages, 'id', 'image');

                $imageArray = [];
                $count = 0;
                foreach ($images as $image) {
                    $imageUrl = ArrayHelper::getValue($model, 'itemImages.'.$count.'.image');
                    if($imageUrl){
                        $imageSmallUrl = self::getCacheImageUrl($imageUrl, 'small');
                        $imageMediumUrl = self::getCacheImageUrl($imageUrl, 'medium');
                        $imageLargeUrl = self::getCacheImageUrl($imageUrl, 'large');

                        $imageArray[$count]['small'] = $imageSmallUrl;
                        $imageArray[$count]['medium'] = $imageMediumUrl;
                        $imageArray[$count]['large'] = $imageLargeUrl;
                    }

                    $count++;
                }
                return $imageArray;
            },
            'costPrice',
            'sellPrice',
            'departmentId' => function ($model) {
                return ArrayHelper::getValue($model->cat->toArray(), 'departmentId');
            },
            'departmentName' => function ($model) {
                return ArrayHelper::getValue($model->cat->toArray(), 'departmentName');
            },
            'departmentName_bd' => function ($model) {
                return ArrayHelper::getValue($model->cat->toArray(), 'departmentName_bd');
            },
            'subdeptId' => function ($model) {
                return ArrayHelper::getValue($model->cat->toArray(), 'subdeptId');
            },
            'subDepartmentName' => function ($model) {
                return ArrayHelper::getValue($model->cat->toArray(), 'subDepartmentName');
            },
            'subDepartmentName_bd' => function ($model) {
                return ArrayHelper::getValue($model->cat->toArray(), 'subDepartmentName_bd');
            },
            'catId',
            'attributescolor',
            'attributessize',
            'categoryName' => function ($model) {
                return ArrayHelper::getValue($model, 'cat.name');
            },
            'categoryName_bd' => function ($model) {
                return ArrayHelper::getValue($model, 'cat.name_bd');
            },
            'brandId',
            'brandName' => function ($model) {
                return ArrayHelper::getValue($model, 'brand.name');
            },
            'brandName_bd' => function ($model) {
                return ArrayHelper::getValue($model, 'brand.name_bd');
            },
            'supplierId',
            'supplierName' => function ($model) {
                return ArrayHelper::getValue($model, 'supplier.name');
            },
            'supplierName_bd' => function ($model) {
                return ArrayHelper::getValue($model, 'supplier.name_bd');
            },
            'taxId',
            'taxRate' => function ($model) {
                return ArrayHelper::getValue($model, 'tax.taxRate');
            },
            'negativeStock',
            'isWeighted',
            'caseCount',
            'isEcommerce',
            'isFeatured',
            'isSales',
            'isParent',
            'specification',
            'specification_bd',
            'shipping_payment',
            'shipping_payment_bd',
            'description',
            'description_bd',
            'attributes' => function ($model) {
                return ItemAttributes::getItemAttributes($model->id);
            },
            'rating' => function ($model) {
                return ArrayHelper::getValue($model, 'itemRating');
            },
            'review' => function ($model) {
                return ArrayHelper::getValue($model, 'itemReviews');
            },
            'crAt',
            'crBy',
            'crByName' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0.username');
            },
            'moAt',
            'moBy',
            'moByName' => function ($model) {
                return ArrayHelper::getValue($model, 'moBy0.username');
            },
            'status',
        ];
    }
}
