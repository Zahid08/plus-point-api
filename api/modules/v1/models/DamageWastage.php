<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%damage_wastage}}".
 *
 * @property string $id
 * @property string $dwNo
 * @property integer $type
 * @property string $branchId
 * @property string $itemId
 * @property double $qty
 * @property string $damDate
 * @property double $costPrice
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * @property Branch $branch
 * @property User $crBy0
 * @property Items $item
 */
class DamageWastage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%damage_wastage}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dwNo'], 'required'],
            [['type', 'branchId', 'itemId', 'crBy', 'moBy', 'status'], 'integer'],
            [['qty', 'costPrice'], 'number'],
            [['damDate', 'crAt', 'moAt'], 'safe'],
            [['dwNo'], 'string', 'max' => 250],
            [['branchId'], 'exist', 'skipOnError' => true, 'targetClass' => Branch::className(), 'targetAttribute' => ['branchId' => 'id']],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['itemId'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['itemId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'dwNo' => Yii::t('app', 'Dw No'),
            'type' => Yii::t('app', 'Type'),
            'branchId' => Yii::t('app', 'Branch ID'),
            'itemId' => Yii::t('app', 'Item ID'),
            'qty' => Yii::t('app', 'Qty'),
            'damDate' => Yii::t('app', 'Dam Date'),
            'costPrice' => Yii::t('app', 'Cost Price'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(Branch::className(), ['id' => 'branchId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::className(), ['id' => 'itemId']);
    }

    /** Get All Active DamageWastage Drop Down List*/
    public static function damageWastageDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }
}
