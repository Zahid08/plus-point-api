<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%purchase_order}}".
 *
 * @property string $id
 * @property string $branchId
 * @property string $supplierId
 * @property string $poNo
 * @property string $poDate
 * @property double $totalQty
 * @property double $totalPrice
 * @property double $totalWeight
 * @property string $remarks
 * @property string $message
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * @property GoodReceiveNote[] $goodReceiveNotes
 * @property PoDetails[] $poDetails
 * @property Branch $branch
 * @property User $crBy0
 * @property User $moBy0
 * @property Supplier $supplier
 */
class PurchaseOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%purchase_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branchId'], 'required'],
            [['branchId', 'supplierId', 'crBy', 'moBy', 'status'], 'integer'],
            [['poDate', 'crAt', 'moAt'], 'safe'],
            [['totalQty', 'totalPrice', 'totalWeight'], 'number'],
            [['poNo', 'remarks', 'message'], 'string', 'max' => 250],
            [['branchId'], 'exist', 'skipOnError' => true, 'targetClass' => Branch::className(), 'targetAttribute' => ['branchId' => 'id']],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
            [['supplierId'], 'exist', 'skipOnError' => true, 'targetClass' => Supplier::className(), 'targetAttribute' => ['supplierId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'branchId' => Yii::t('app', 'Branch ID'),
            'supplierId' => Yii::t('app', 'Supplier ID'),
            'poNo' => Yii::t('app', 'Po No'),
            'poDate' => Yii::t('app', 'Po Date'),
            'totalQty' => Yii::t('app', 'Total Qty'),
            'totalPrice' => Yii::t('app', 'Total Price'),
            'totalWeight' => Yii::t('app', 'Total Weight'),
            'remarks' => Yii::t('app', 'Remarks'),
            'message' => Yii::t('app', 'Message'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodReceiveNotes()
    {
        return $this->hasMany(GoodReceiveNote::className(), ['poId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoDetails()
    {
        return $this->hasMany(PoDetails::className(), ['poId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(Branch::className(), ['id' => 'branchId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Supplier::className(), ['id' => 'supplierId']);
    }

    /** Get All Active PurchaseOrder Drop Down List*/
    public static function purchaseOrderDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }

    public function fields()
    {
        return [
            'id',
            'branchId',
            'branchName' => function ($model) {
                return ArrayHelper::getValue($model, 'branch.name');
            },
            'supplierId',
            'poNo',
            'poDate',
            'totalQty',
            'totalPrice',
            'totalWeight',
            'remarks',
            'message',
            'crAt',
            'crBy',
            'moAt',
            'moBy',
            'status',
        ];
    }

    public function extraFields()
    {
        return [
            'supplierInfo' => function ($model) {
                return $model->supplier;
            },
            'orderItems' => function ($model) {
                return $model->poDetails;
            },
            'goodReceiveNotes' => function ($model) {
                return $model->goodReceiveNotes;
            },
        ];
    }
}
