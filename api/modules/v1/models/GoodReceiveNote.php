<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%good_receive_note}}".
 *
 * @property string $id
 * @property string $supplierId
 * @property string $poId
 * @property string $grnNo
 * @property string $grnDate
 * @property double $totalQty
 * @property double $totalPrice
 * @property double $totalDiscount
 * @property double $totalWeight
 * @property string $remarks
 * @property string $message
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * @property User $crBy0
 * @property PurchaseOrder $po
 * @property Supplier $supplier
 * @property User $moBy0
 * @property Stock[] $stocks
 * @property SupplierDeposit[] $supplierDeposits
 */
class GoodReceiveNote extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%good_receive_note}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['supplierId', 'poId', 'crBy', 'moBy', 'status'], 'integer'],
            [['grnDate', 'crAt', 'moAt'], 'safe'],
            [['totalQty', 'totalPrice', 'totalDiscount', 'totalWeight'], 'number'],
            [['grnNo', 'remarks', 'message'], 'string', 'max' => 250],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['poId'], 'exist', 'skipOnError' => true, 'targetClass' => PurchaseOrder::className(), 'targetAttribute' => ['poId' => 'id']],
            [['supplierId'], 'exist', 'skipOnError' => true, 'targetClass' => Supplier::className(), 'targetAttribute' => ['supplierId' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'supplierId' => Yii::t('app', 'Supplier ID'),
            'poId' => Yii::t('app', 'Po ID'),
            'grnNo' => Yii::t('app', 'Grn No'),
            'grnDate' => Yii::t('app', 'Grn Date'),
            'totalQty' => Yii::t('app', 'Total Qty'),
            'totalPrice' => Yii::t('app', 'Total Price'),
            'totalDiscount' => Yii::t('app', 'Total Discount'),
            'totalWeight' => Yii::t('app', 'Total Weight'),
            'remarks' => Yii::t('app', 'Remarks'),
            'message' => Yii::t('app', 'Message'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPo()
    {
        return $this->hasOne(PurchaseOrder::className(), ['id' => 'poId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Supplier::className(), ['id' => 'supplierId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStocks()
    {
        return $this->hasMany(Stock::className(), ['grnId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplierDeposits()
    {
        return $this->hasMany(SupplierDeposit::className(), ['grnId' => 'id']);
    }

    /** Get All Active GoodReceiveNote Drop Down List*/
    public static function goodReceiveNoteDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }
}
