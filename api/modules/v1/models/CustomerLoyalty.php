<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%customer_loyalty}}".
 *
 * @property string $id
 * @property string $custId
 * @property string $salesId
 * @property double $loyaltyAmount
 * @property string $remarks
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $rank
 *
 * @property User $crBy0
 * @property Customer $cust
 * @property User $moBy0
 * @property SalesInvoice $sales
 */
class CustomerLoyalty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customer_loyalty}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['custId'], 'required'],
            [['custId', 'salesId', 'status', 'crBy', 'moBy', 'rank'], 'integer'],
            [['loyaltyAmount'], 'number'],
            [['crAt', 'moAt'], 'safe'],
            [['remarks'], 'string', 'max' => 250],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['custId'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['custId' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
            [['salesId'], 'exist', 'skipOnError' => true, 'targetClass' => SalesInvoice::className(), 'targetAttribute' => ['salesId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'custId' => Yii::t('app', 'Cust ID'),
            'salesId' => Yii::t('app', 'Sales ID'),
            'loyaltyAmount' => Yii::t('app', 'Loyalty Amount'),
            'remarks' => Yii::t('app', 'Remarks'),
            'status' => Yii::t('app', 'Status'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'rank' => Yii::t('app', 'Rank'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCust()
    {
        return $this->hasOne(Customer::className(), ['id' => 'custId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSales()
    {
        return $this->hasOne(SalesInvoice::className(), ['id' => 'salesId']);
    }

    /** Get All Active CustomerLoyalty Drop Down List*/
    public static function customerLoyaltyDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }
}
