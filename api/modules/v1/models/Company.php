<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%company}}".
 *
 * @property string $id
 * @property string $packageId
 * @property string $orgIdPrefix
 * @property string $name
 * @property string $domain
 * @property string $email
 * @property string $favicon
 * @property string $logo
 * @property string $theme
 * @property string $skin
 * @property string $detailsinfo
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * @property User $crBy0
 * @property User $moBy0
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['packageId', 'status', 'crBy', 'moBy'], 'integer'],
            [['domain', 'email', 'favicon'], 'required'],
            [['detailsinfo'], 'string'],
            [['crAt', 'moAt'], 'safe'],
            [['orgIdPrefix'], 'string', 'max' => 5],
            [['name', 'domain', 'email', 'favicon', 'logo', 'theme', 'skin'], 'string', 'max' => 150],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'packageId' => Yii::t('app', 'Package ID'),
            'orgIdPrefix' => Yii::t('app', 'Org Id Prefix'),
            'name' => Yii::t('app', 'Name'),
            'domain' => Yii::t('app', 'Domain'),
            'email' => Yii::t('app', 'Email'),
            'favicon' => Yii::t('app', 'Favicon'),
            'logo' => Yii::t('app', 'Logo'),
            'theme' => Yii::t('app', 'Theme'),
            'skin' => Yii::t('app', 'Skin'),
            'detailsinfo' => Yii::t('app', 'Detailsinfo'),
            'status' => Yii::t('app', 'Status'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
        ];
    }


    /*********** Branch Information ***************/
    public function getBranch(){


        $model = Branch::find()
            ->where('isDefault =:isDefault', [':isDefault' => 1])
            ->one();
        if(empty($model)){
            $model = array('id' => 1);
        }
        return $model;
    }

    /*********** Branch Information ***************/
    public function getSeo(){
        $model = Seo::find()
            ->where('status =:status', [':status' => 1])
            ->all();

        return $model;
    }




    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /** Get All Active Company Drop Down List*/
    public static function companyDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }

    public function fields()
    {

        $fields = parent::fields();

        $additionalFields = [
            'branchinfo' => function ($model) {
                return ArrayHelper::getValue($model, 'branch');
            },
            'seoinfo' => function ($model) {
                return ArrayHelper::getValue($model, 'seo');
            },
        ];

        $result = ArrayHelper::merge($fields, $additionalFields);

        return $result;
    }

    public function extraFields()
    {
       /* return [
            'userInfosss' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },
        ];*/
    }
}
