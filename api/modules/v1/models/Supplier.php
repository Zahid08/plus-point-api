<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%supplier}}".
 *
 * @property string $id
 * @property string $suppId
 * @property string $name
 * @property string $name_bd
 * @property string $address
 * @property string $city
 * @property string $email
 * @property string $phone
 * @property string $crType
 * @property integer $creditDay
 * @property integer $consignmentDay
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $rank
 *
 * @property GoodReceiveNote[] $goodReceiveNotes
 * @property Items[] $items
 * @property PurchaseOrder[] $purchaseOrders
 * @property User $crBy0
 * @property User $moBy0
 * @property SupplierDeposit[] $supplierDeposits
 * @property SupplierWidraw[] $supplierWidraws
 */
class Supplier extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%supplier}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['suppId'], 'required'],
            [['crType'], 'string'],
            [['creditDay', 'consignmentDay', 'status', 'crBy', 'moBy', 'rank'], 'integer'],
            [['crAt', 'moAt'], 'safe'],
            [['suppId', 'city', 'email', 'phone'], 'string', 'max' => 150],
            [['name', 'name_bd'], 'string', 'max' => 50],
            [['address'], 'string', 'max' => 250],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'suppId' => Yii::t('app', 'Supp ID'),
            'name' => Yii::t('app', 'Name'),
            'name_bd' => Yii::t('app', 'Name (বাংলা)'),
            'address' => Yii::t('app', 'Address'),
            'city' => Yii::t('app', 'City'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'crType' => Yii::t('app', 'Cr Type'),
            'creditDay' => Yii::t('app', 'Credit Day'),
            'consignmentDay' => Yii::t('app', 'Consignment Day'),
            'status' => Yii::t('app', 'Status'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'rank' => Yii::t('app', 'Rank'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodReceiveNotes()
    {
        return $this->hasMany(GoodReceiveNote::className(), ['supplierId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Items::className(), ['supplierId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrders()
    {
        return $this->hasMany(PurchaseOrder::className(), ['supplierId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplierDeposits()
    {
        return $this->hasMany(SupplierDeposit::className(), ['supId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplierWidraws()
    {
        return $this->hasMany(SupplierWidraw::className(), ['supId' => 'id']);
    }

    /** Get All Active Supplier Drop Down List*/
    public static function supplierDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }
}
