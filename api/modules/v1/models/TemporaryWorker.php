<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%temporary_worker}}".
 *
 * @property string $id
 * @property string $twNo
 * @property string $branchId
 * @property string $wkId
 * @property string $itemId
 * @property double $qty
 * @property double $rqty
 * @property string $twDate
 * @property double $costPrice
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * @property Branch $branch
 * @property User $crBy0
 * @property Items $item
 * @property User $wk
 */
class TemporaryWorker extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%temporary_worker}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['twNo'], 'required'],
            [['branchId', 'wkId', 'itemId', 'crBy', 'moBy', 'status'], 'integer'],
            [['qty', 'rqty', 'costPrice'], 'number'],
            [['twDate', 'crAt', 'moAt'], 'safe'],
            [['twNo'], 'string', 'max' => 250],
            [['branchId'], 'exist', 'skipOnError' => true, 'targetClass' => Branch::className(), 'targetAttribute' => ['branchId' => 'id']],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['itemId'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['itemId' => 'id']],
            [['wkId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['wkId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'twNo' => Yii::t('app', 'Tw No'),
            'branchId' => Yii::t('app', 'Branch ID'),
            'wkId' => Yii::t('app', 'Wk ID'),
            'itemId' => Yii::t('app', 'Item ID'),
            'qty' => Yii::t('app', 'Qty'),
            'rqty' => Yii::t('app', 'Rqty'),
            'twDate' => Yii::t('app', 'Tw Date'),
            'costPrice' => Yii::t('app', 'Cost Price'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(Branch::className(), ['id' => 'branchId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::className(), ['id' => 'itemId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWk()
    {
        return $this->hasOne(User::className(), ['id' => 'wkId']);
    }

    /** Get All Active TemporaryWorker Drop Down List*/
    public static function temporaryWorkerDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }
}
