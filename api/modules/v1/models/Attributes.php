<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%attributes}}".
 *
 * @property string $id
 * @property integer $attTypeId
 * @property string $name
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $rank
 *
 * @property AttributesType $attType
 * @property User $crBy0
 * @property User $moBy0
 * @property ItemAttributes[] $itemAttributes
 */
class Attributes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%attributes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attTypeId', 'status', 'crBy', 'moBy', 'rank'], 'integer'],
            [['crAt', 'moAt'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['attTypeId'], 'exist', 'skipOnError' => true, 'targetClass' => AttributesType::className(), 'targetAttribute' => ['attTypeId' => 'id']],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'attTypeId' => Yii::t('app', 'Att Type ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'rank' => Yii::t('app', 'Rank'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttType()
    {
        return $this->hasOne(AttributesType::className(), ['id' => 'attTypeId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemAttributes()
    {
        return $this->hasMany(ItemAttributes::className(), ['attId' => 'id']);
    }

    /** Get All Active Attributes Drop Down List*/
    public static function attributesDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    public static function getAttributesInfo($id)
    {
        return self::find()
            ->where('id =:id', [':id' => $id])
            ->one();
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }
}
