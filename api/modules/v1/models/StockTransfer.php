<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%stock_transfer}}".
 *
 * @property string $id
 * @property string $transferNo
 * @property string $fromBranchId
 * @property string $toBranchId
 * @property string $itemId
 * @property double $qty
 * @property string $stockDate
 * @property double $costPrice
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * @property User $crBy0
 * @property Branch $fromBranch
 * @property Items $item
 * @property Branch $toBranch
 */
class StockTransfer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%stock_transfer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['transferNo'], 'required'],
            [['fromBranchId', 'toBranchId', 'itemId', 'crBy', 'moBy', 'status'], 'integer'],
            [['qty', 'costPrice'], 'number'],
            [['stockDate', 'crAt', 'moAt'], 'safe'],
            [['transferNo'], 'string', 'max' => 250],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['fromBranchId'], 'exist', 'skipOnError' => true, 'targetClass' => Branch::className(), 'targetAttribute' => ['fromBranchId' => 'id']],
            [['itemId'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['itemId' => 'id']],
            [['toBranchId'], 'exist', 'skipOnError' => true, 'targetClass' => Branch::className(), 'targetAttribute' => ['toBranchId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'transferNo' => Yii::t('app', 'Transfer No'),
            'fromBranchId' => Yii::t('app', 'From Branch ID'),
            'toBranchId' => Yii::t('app', 'To Branch ID'),
            'itemId' => Yii::t('app', 'Item ID'),
            'qty' => Yii::t('app', 'Qty'),
            'stockDate' => Yii::t('app', 'Stock Date'),
            'costPrice' => Yii::t('app', 'Cost Price'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromBranch()
    {
        return $this->hasOne(Branch::className(), ['id' => 'fromBranchId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::className(), ['id' => 'itemId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToBranch()
    {
        return $this->hasOne(Branch::className(), ['id' => 'toBranchId']);
    }

    /** Get All Active StockTransfer Drop Down List*/
    public static function stockTransferDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }
}
