<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%item_review}}".
 *
 * @property string $id
 * @property string $itemId
 * @property integer $rating
 * @property string $name
 * @property string $email
 * @property string $comment
 * @property string $crAt
 * @property string $status
 *
 * @property Items $item
 */
class ItemReview extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%item_review}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['itemId', 'rating', 'status'], 'integer'],
            [['rating'], 'required'],
            [['comment'], 'string'],
            [['crAt'], 'safe'],
            [['name', 'email'], 'string', 'max' => 150],
            [['itemId'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['itemId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'itemId' => Yii::t('app', 'Item ID'),
            'rating' => Yii::t('app', 'Rating'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'comment' => Yii::t('app', 'Comment'),
            'crAt' => Yii::t('app', 'Cr At'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::className(), ['id' => 'itemId']);
    }

    /** Get All Active ItemReview Drop Down List*/
    public static function itemReviewDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crAt = date('Y-m-d h:i:s');
                $this->status = 2;
            }
            else
            {

            }
            return true;
        }
        else return false;
    }
}
