<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%shipping_method}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $day
 * @property string $image
 * @property string $description
 * @property string $isPercent
 * @property double $price
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * @property User $crBy0
 * @property User $moBy0
 */
class ShippingMethod extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shipping_method}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'price'], 'required'],
            [['image', 'description', 'isPercent'], 'string'],
            [['price'], 'number'],
            [['status', 'crBy', 'moBy'], 'integer'],
            [['crAt', 'moAt'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['day'], 'string', 'max' => 50],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'day' => Yii::t('app', 'Day'),
            'image' => Yii::t('app', 'Image'),
            'description' => Yii::t('app', 'Description'),
            'isPercent' => Yii::t('app', 'Is Percent'),
            'price' => Yii::t('app', 'Price'),
            'status' => Yii::t('app', 'Status'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /** Get All Active ShippingMethod Drop Down List*/
    public static function shippingMethodDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }

    public function fields()
    {
        $fields = parent::fields();

        $additionalFields = [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];

        $result = ArrayHelper::merge($fields, $additionalFields);

        return $result;
    }

    public function extraFields()
    {
        return [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];
    }
}
