<?php

namespace api\modules\v1\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\GoodReceiveNote;
use api\helpers\CommonHelper;

/**
 * GoodReceiveNoteSearch represents the model behind the search form about `api\modules\v1\models\GoodReceiveNote`.
 */
class GoodReceiveNoteSearch extends GoodReceiveNote
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'supplierId', 'poId', 'crBy', 'moBy', 'status'], 'integer'],
            [['grnNo', 'grnDate', 'remarks', 'message', 'crAt', 'moAt'], 'safe'],
            [['totalQty', 'totalPrice', 'totalDiscount', 'totalWeight'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GoodReceiveNote::find();

        // add conditions that should always apply here
        $pageSize = isset($params['pageSize']) ? intval($params['pageSize']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'supplierId' => $this->supplierId,
            'poId' => $this->poId,
            'grnDate' => $this->grnDate,
            'totalQty' => $this->totalQty,
            'totalPrice' => $this->totalPrice,
            'totalDiscount' => $this->totalDiscount,
            'totalWeight' => $this->totalWeight,
            'crAt' => $this->crAt,
            'crBy' => $this->crBy,
            'moAt' => $this->moAt,
            'moBy' => $this->moBy,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'grnNo', $this->grnNo])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}
