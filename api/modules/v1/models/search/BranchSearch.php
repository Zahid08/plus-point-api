<?php

namespace api\modules\v1\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\Branch;
use api\helpers\CommonHelper;

/**
 * BranchSearch represents the model behind the search form about `api\modules\v1\models\Branch`.
 */
class BranchSearch extends Branch
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'isDefault', 'isInvoiceFull', 'isDueInvoice', 'isSpecialDiscount', 'isInstantDiscount', 'crBy', 'moBy'], 'integer'],
            [['branchIdPrefix', 'name', 'addressline', 'city', 'postalcode', 'phone', 'fax', 'email', 'vatrn', 'crAt', 'moAt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Branch::find();

        // add conditions that should always apply here
        $pageSize = isset($params['pageSize']) ? intval($params['pageSize']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'isDefault' => $this->isDefault,
            'isInvoiceFull' => $this->isInvoiceFull,
            'isDueInvoice' => $this->isDueInvoice,
            'isSpecialDiscount' => $this->isSpecialDiscount,
            'isInstantDiscount' => $this->isInstantDiscount,
            'crAt' => $this->crAt,
            'crBy' => $this->crBy,
            'moAt' => $this->moAt,
            'moBy' => $this->moBy,
        ]);

        $query->andFilterWhere(['like', 'branchIdPrefix', $this->branchIdPrefix])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'addressline', $this->addressline])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'postalcode', $this->postalcode])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'vatrn', $this->vatrn]);

        return $dataProvider;
    }
}
