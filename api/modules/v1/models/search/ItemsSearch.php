<?php

namespace api\modules\v1\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\Items;
use api\helpers\CommonHelper;

/**
 * ItemsSearch represents the model behind the search form about `api\modules\v1\models\Items`.
 */
class ItemsSearch extends Items
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'catId', 'brandId', 'supplierId', 'taxId', 'caseCount', 'isEcommerce', 'isFeatured', 'isSales', 'isParent', 'crBy', 'moBy', 'status'], 'integer'],
            [['itemCode', 'barCode', 'itemName', 'itemName_bd', 'negativeStock', 'isWeighted', 'specification', 'specification_bd', 'shipping_payment', 'shipping_payment_bd', 'description', 'description_bd', 'crAt', 'moAt'], 'safe'],
            [['costPrice', 'sellPrice'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Items::find();

        // add conditions that should always apply here
        $pageSize = isset($params['pageSize']) ? intval($params['pageSize']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'costPrice' => $this->costPrice,
            'sellPrice' => $this->sellPrice,
            'catId' => $this->catId,
            'brandId' => $this->brandId,
            'supplierId' => $this->supplierId,
            'taxId' => $this->taxId,
            'caseCount' => $this->caseCount,
            'isEcommerce' => $this->isEcommerce,
            'isFeatured' => $this->isFeatured,
            'isSales' => $this->isSales,
            'isParent' => $this->isParent,
            'crAt' => $this->crAt,
            'crBy' => $this->crBy,
            'moAt' => $this->moAt,
            'moBy' => $this->moBy,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'itemCode', $this->itemCode])
            ->andFilterWhere(['like', 'barCode', $this->barCode])
            ->andFilterWhere(['like', 'itemName', $this->itemName])
            ->andFilterWhere(['like', 'itemName_bd', $this->itemName_bd])
            ->andFilterWhere(['like', 'negativeStock', $this->negativeStock])
            ->andFilterWhere(['like', 'isWeighted', $this->isWeighted])
            ->andFilterWhere(['like', 'specification', $this->specification])
            ->andFilterWhere(['like', 'specification_bd', $this->specification_bd])
            ->andFilterWhere(['like', 'shipping_payment', $this->shipping_payment])
            ->andFilterWhere(['like', 'shipping_payment_bd', $this->shipping_payment_bd])
            ->andFilterWhere(['like', 'description_bd', $this->description_bd])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
