<?php

namespace api\modules\v1\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\ItemParents;
use api\helpers\CommonHelper;

/**
 * ItemParentsSearch represents the model behind the search form about `api\modules\v1\models\ItemParents`.
 */
class ItemParentsSearch extends ItemParents
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'itemId', 'parentId', 'status', 'rank', 'crBy'], 'integer'],
            [['qty'], 'number'],
            [['crAt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ItemParents::find();

        // add conditions that should always apply here
        $pageSize = isset($params['pageSize']) ? intval($params['pageSize']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'itemId' => $this->itemId,
            'parentId' => $this->parentId,
            'qty' => $this->qty,
            'status' => $this->status,
            'rank' => $this->rank,
            'crAt' => $this->crAt,
            'crBy' => $this->crBy,
        ]);

        return $dataProvider;
    }
}
