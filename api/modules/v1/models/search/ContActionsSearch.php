<?php

namespace api\modules\v1\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\ContActions;
use api\helpers\CommonHelper;

/**
 * ContActionsSearch represents the model behind the search form about `api\modules\v1\models\ContActions`.
 */
class ContActionsSearch extends ContActions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contId', 'layout', 'status', 'crBy', 'moBy'], 'integer'],
            [['name', 'description', 'crAt', 'moAt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContActions::find();

        // add conditions that should always apply here
        $pageSize = isset($params['pageSize']) ? intval($params['pageSize']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contId' => $this->contId,
            'layout' => $this->layout,
            'status' => $this->status,
            'crBy' => $this->crBy,
            'crAt' => $this->crAt,
            'moBy' => $this->moBy,
            'moAt' => $this->moAt,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
