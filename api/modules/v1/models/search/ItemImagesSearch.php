<?php

namespace api\modules\v1\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\ItemImages;
use api\helpers\CommonHelper;

/**
 * ItemImagesSearch represents the model behind the search form about `api\modules\v1\models\ItemImages`.
 */
class ItemImagesSearch extends ItemImages
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'itemId', 'status', 'rank', 'crBy'], 'integer'],
            [['image', 'crAt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ItemImages::find();

        // add conditions that should always apply here
        $pageSize = isset($params['pageSize']) ? intval($params['pageSize']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'itemId' => $this->itemId,
            'status' => $this->status,
            'rank' => $this->rank,
            'crAt' => $this->crAt,
            'crBy' => $this->crBy,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
