<?php

namespace api\modules\v1\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\SupplierDeposit;
use api\helpers\CommonHelper;

/**
 * SupplierDepositSearch represents the model behind the search form about `api\modules\v1\models\SupplierDeposit`.
 */
class SupplierDepositSearch extends SupplierDeposit
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'branchId', 'supId', 'grnId', 'status', 'crBy', 'moBy', 'rank'], 'integer'],
            [['amount'], 'number'],
            [['remarks', 'crAt', 'moAt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SupplierDeposit::find();

        // add conditions that should always apply here
        $pageSize = isset($params['pageSize']) ? intval($params['pageSize']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'branchId' => $this->branchId,
            'supId' => $this->supId,
            'grnId' => $this->grnId,
            'amount' => $this->amount,
            'status' => $this->status,
            'crAt' => $this->crAt,
            'crBy' => $this->crBy,
            'moAt' => $this->moAt,
            'moBy' => $this->moBy,
            'rank' => $this->rank,
        ]);

        $query->andFilterWhere(['like', 'remarks', $this->remarks]);

        return $dataProvider;
    }
}
