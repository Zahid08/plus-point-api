<?php

namespace api\modules\v1\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\ShippingMethod;
use api\helpers\CommonHelper;

/**
 * ShippingMethodSearch represents the model behind the search form about `api\modules\v1\models\ShippingMethod`.
 */
class ShippingMethodSearch extends ShippingMethod
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'crBy', 'moBy'], 'integer'],
            [['title', 'day', 'image', 'description', 'isPercent', 'crAt', 'moAt'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShippingMethod::find();

        // add conditions that should always apply here
        $pageSize = isset($params['limit']) ? intval($params['limit']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        // Filter
        if(isset($params['filter']) && !empty($params['filter'])){
            $filter = new ActiveDataFilter([
                'searchModel' => self::class,
            ]);

            $filterCondition = null;

            $filterParams = json_decode($params['filter'], true);

            $filter->setFilter($filterParams);

            if ($filter->load($filterParams)) {
                $filterCondition = $filter->build();
                if ($filterCondition === false) {
                    // Serializer would get errors out of it
                    return $filter;
                }
            }

            if ($filterCondition !== null) {
                $query->andWhere($filterCondition);
            }
        }

        // Search
        if(isset($params['search']) && !empty($params['search'])){
            $query->andWhere(['or',
                ['like', 'name', $params['search']],
                ['like', 'domain', $params['search']],
                ['like', 'detailsinfo', $params['search']],
            ]);
        }

        return $dataProvider;
    }
}
