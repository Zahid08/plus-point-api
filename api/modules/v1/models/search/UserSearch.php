<?php

namespace api\modules\v1\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\User;
use api\helpers\CommonHelper;

/**
 * UserSearch represents the model behind the search form about `api\modules\v1\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'branchId', 'supplierId', 'custId', 'totalLoginTime', 'status'], 'integer'],
            [['username', 'password', 'salt', 'ipAddress', 'browser', 'os', 'lastlogin', 'lastlogout'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here
        $pageSize = isset($params['pageSize']) ? intval($params['pageSize']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'branchId' => $this->branchId,
            'supplierId' => $this->supplierId,
            'custId' => $this->custId,
            'totalLoginTime' => $this->totalLoginTime,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'salt', $this->salt])
            ->andFilterWhere(['like', 'ipAddress', $this->ipAddress])
            ->andFilterWhere(['like', 'browser', $this->browser])
            ->andFilterWhere(['like', 'os', $this->os])
            ->andFilterWhere(['like', 'lastlogin', $this->lastlogin])
            ->andFilterWhere(['like', 'lastlogout', $this->lastlogout]);

        return $dataProvider;
    }
}
