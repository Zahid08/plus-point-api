<?php

namespace api\modules\v1\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\CustomScripts;
use api\helpers\CommonHelper;

/**
 * CustomScriptsSearch represents the model behind the search form about `api\modules\v1\models\CustomScripts`.
 */
class CustomScriptsSearch extends CustomScripts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'crBy', 'moBy', 'status'], 'integer'],
            [['title', 'script', 'position', 'crAt', 'moAt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomScripts::find();

        // add conditions that should always apply here
        $pageSize = isset($params['pageSize']) ? intval($params['pageSize']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'crAt' => $this->crAt,
            'crBy' => $this->crBy,
            'moAt' => $this->moAt,
            'moBy' => $this->moBy,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'script', $this->script])
            ->andFilterWhere(['like', 'position', $this->position]);

        return $dataProvider;
    }
}
