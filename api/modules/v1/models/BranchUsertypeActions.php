<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%branch_usertype_actions}}".
 *
 * @property string $id
 * @property string $usertypeId
 * @property string $contActionsId
 * @property integer $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * @property ContActions $contActions
 * @property User $crBy0
 * @property User $moBy0
 * @property UserType $usertype
 */
class BranchUsertypeActions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%branch_usertype_actions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usertypeId', 'contActionsId', 'status', 'crBy', 'moBy'], 'integer'],
            [['crAt', 'moAt'], 'safe'],
            [['contActionsId'], 'exist', 'skipOnError' => true, 'targetClass' => ContActions::className(), 'targetAttribute' => ['contActionsId' => 'id']],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
            [['usertypeId'], 'exist', 'skipOnError' => true, 'targetClass' => UserType::className(), 'targetAttribute' => ['usertypeId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'usertypeId' => Yii::t('app', 'Usertype ID'),
            'contActionsId' => Yii::t('app', 'Cont Actions ID'),
            'status' => Yii::t('app', 'Status'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContActions()
    {
        return $this->hasOne(ContActions::className(), ['id' => 'contActionsId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsertype()
    {
        return $this->hasOne(UserType::className(), ['id' => 'usertypeId']);
    }

    /** Get All Active BranchUsertypeActions Drop Down List*/
    public static function branchUsertypeActionsDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }
}
