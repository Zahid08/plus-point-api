<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%subdepartment}}".
 *
 * @property string $id
 * @property string $deptId
 * @property string $name
 * @property string $name_bd
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $rank
 *
 * @property Category[] $categories
 * @property InvSettings[] $invSettings
 * @property User $crBy0
 * @property Department $dept
 * @property User $moBy0
 */
class Subdepartment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%subdepartment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['deptId', 'status', 'crBy', 'moBy', 'rank'], 'integer'],
            [['crAt', 'moAt'], 'safe'],
            [['name', 'name_bd'], 'string', 'max' => 50],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['deptId'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['deptId' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'deptId' => Yii::t('app', 'Dept ID'),
            'name' => Yii::t('app', 'Name'),
            'name_bd' => Yii::t('app', 'Name (বাংলা)'),
            'status' => Yii::t('app', 'Status'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'rank' => Yii::t('app', 'Rank'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['subdeptId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvSettings()
    {
        return $this->hasMany(InvSettings::className(), ['subDeptId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDept()
    {
        return $this->hasOne(Department::className(), ['id' => 'deptId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /** Get All Active Subdepartment Drop Down List*/
    public static function subdepartmentDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }
}
