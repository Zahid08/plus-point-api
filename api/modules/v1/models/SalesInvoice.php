<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%sales_invoice}}".
 *
 * @property string $id
 * @property string $invNo
 * @property string $branchId
 * @property string $custId
 * @property string $orderDate
 * @property string $delivaryDate
 * @property double $totalQty
 * @property double $totalWeight
 * @property double $totalPrice
 * @property double $totalDiscount
 * @property double $instantDiscount
 * @property double $totalTax
 * @property double $totalShipping
 * @property double $cashPaid
 * @property double $cardPaid
 * @property double $crbalPaid
 * @property double $loyaltyPaid
 * @property string $cardId
 * @property string $cardNo
 * @property string $cardAppNo
 * @property double $totalChangeAmount
 * @property double $spDiscount
 * @property string $couponId
 * @property string $remarks
 * @property string $message
 * @property integer $isEcommerce
 * @property string $billingAddress
 * @property string $shippingAddress
 * @property string $shippingId
 * @property string $paymentId
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * @property CustomerLoyalty[] $customerLoyalties
 * @property SalesDetails[] $salesDetails
 * @property Customer $customer
 * @property Branch $branch
 * @property User $crBy0
 * @property User $moBy0
 * @property SalesReturn[] $salesReturns
 */
class SalesInvoice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sales_invoice}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branchId', 'custId', 'cardId', 'couponId', 'isEcommerce', 'shippingId', 'paymentId', 'crBy', 'moBy', 'status'], 'integer'],
            [['custId', 'couponId'], 'required'],
            [['orderDate', 'delivaryDate', 'crAt', 'moAt'], 'safe'],
            [['totalQty', 'totalWeight', 'totalPrice', 'totalDiscount', 'instantDiscount', 'totalTax', 'totalShipping', 'cashPaid', 'cardPaid', 'crbalPaid', 'loyaltyPaid', 'totalChangeAmount', 'spDiscount'], 'number'],
            [['billingAddress', 'shippingAddress'], 'string'],
            [['invNo', 'remarks', 'message'], 'string', 'max' => 250],
            [['cardNo', 'cardAppNo'], 'string', 'max' => 150],
            [['branchId'], 'exist', 'skipOnError' => true, 'targetClass' => Branch::className(), 'targetAttribute' => ['branchId' => 'id']],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'invNo' => Yii::t('app', 'Inv No'),
            'branchId' => Yii::t('app', 'Branch ID'),
            'custId' => Yii::t('app', 'Cust ID'),
            'orderDate' => Yii::t('app', 'Order Date'),
            'delivaryDate' => Yii::t('app', 'Delivary Date'),
            'totalQty' => Yii::t('app', 'Total Qty'),
            'totalWeight' => Yii::t('app', 'Total Weight'),
            'totalPrice' => Yii::t('app', 'Total Price'),
            'totalDiscount' => Yii::t('app', 'Total Discount'),
            'instantDiscount' => Yii::t('app', 'Instant Discount'),
            'totalTax' => Yii::t('app', 'Total Tax'),
            'totalShipping' => Yii::t('app', 'Total Shipping'),
            'cashPaid' => Yii::t('app', 'Cash Paid'),
            'cardPaid' => Yii::t('app', 'Card Paid'),
            'crbalPaid' => Yii::t('app', 'Crbal Paid'),
            'loyaltyPaid' => Yii::t('app', 'Loyalty Paid'),
            'cardId' => Yii::t('app', 'Card ID'),
            'cardNo' => Yii::t('app', 'Card No'),
            'cardAppNo' => Yii::t('app', 'Card App No'),
            'totalChangeAmount' => Yii::t('app', 'Total Change Amount'),
            'spDiscount' => Yii::t('app', 'Sp Discount'),
            'couponId' => Yii::t('app', 'Coupon ID'),
            'remarks' => Yii::t('app', 'Remarks'),
            'message' => Yii::t('app', 'Message'),
            'isEcommerce' => Yii::t('app', 'Is Ecommerce'),
            'billingAddress' => Yii::t('app', 'Billing Address'),
            'shippingAddress' => Yii::t('app', 'Shipping Address'),
            'shippingId' => Yii::t('app', 'Shipping ID'),
            'paymentId' => Yii::t('app', 'Payment ID'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerLoyalties()
    {
        return $this->hasMany(CustomerLoyalty::className(), ['salesId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesDetails()
    {
        return $this->hasMany(SalesDetails::className(), ['salesId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(Branch::className(), ['id' => 'branchId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'custId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesReturns()
    {
        return $this->hasMany(SalesReturn::className(), ['salesId' => 'id']);
    }

    /** Get All Active SalesInvoice Drop Down List*/
    public static function salesInvoiceDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
                $this->orderDate = date('Y-m-d h:i:s');
                $this->branchId = 2;
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }

    public function fields()
    {
        $fields = parent::fields();

        $additionalFields = [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];

        $result = ArrayHelper::merge($fields, $additionalFields);

        return $result;
    }

    public function extraFields()
    {
        return [
            'customer' => function ($model) {
                return ArrayHelper::getValue($model, 'customer');
            },
            'branch' => function ($model) {
                return ArrayHelper::getValue($model, 'branch');
            },
            'salesDetails' => function ($model) {
                return ArrayHelper::getValue($model, 'salesDetails');
            },
            'salesReturns' => function ($model) {
                return ArrayHelper::getValue($model, 'salesReturns');
            },
            'customerLoyalties' => function ($model) {
                return ArrayHelper::getValue($model, 'customerLoyalties');
            },
        ];
    }
}
