<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%customer}}".
 *
 * @property string $id
 * @property string $custId
 * @property integer $custType
 * @property string $title
 * @property string $name
 * @property string $gender
 * @property string $addressline
 * @property string $city
 * @property string $phone
 * @property string $email
 * @property string $crLimit
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * @property User $crBy0
 * @property CustomerType $custType0
 * @property User $moBy0
 * @property CustomerDeposit[] $customerDeposits
 * @property CustomerLoyalty[] $customerLoyalties
 * @property Wishlist[] $wishlists
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['custType', 'crLimit', 'status', 'crBy', 'moBy'], 'integer'],
            [['title', 'gender'], 'string'],
            [['crAt', 'moAt'], 'safe'],
            [['custId', 'city'], 'string', 'max' => 150],
            [['name', 'addressline'], 'string', 'max' => 255],
            [['phone', 'email'], 'string', 'max' => 155],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['custType'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerType::className(), 'targetAttribute' => ['custType' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'custId' => Yii::t('app', 'Cust ID'),
            'custType' => Yii::t('app', 'Cust Type'),
            'title' => Yii::t('app', 'Title'),
            'name' => Yii::t('app', 'Name'),
            'gender' => Yii::t('app', 'Gender'),
            'addressline' => Yii::t('app', 'Addressline'),
            'city' => Yii::t('app', 'City'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'crLimit' => Yii::t('app', 'Cr Limit'),
            'status' => Yii::t('app', 'Status'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustType0()
    {
        return $this->hasOne(CustomerType::className(), ['id' => 'custType']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerDeposits()
    {
        return $this->hasMany(CustomerDeposit::className(), ['custId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerLoyalties()
    {
        return $this->hasMany(CustomerLoyalty::className(), ['custId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWishlists()
    {
        return $this->hasMany(Wishlist::className(), ['custId' => 'id']);
    }

    /** Get All Active Customer Drop Down List*/
    public static function customerDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    public static function getCustomerInfoByEmail($email)
    {
        return self::find()
            ->where('email =:email', [':email' => $email])
            /*->andWhere(['status' => CommonHelper::STATUS_ACTIVE])*/
            ->one();
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }

    public function fields()
    {
        $fields = parent::fields();

        $additionalFields = [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];

        $result = ArrayHelper::merge($fields, $additionalFields);

        return $result;
    }

    public function extraFields()
    {
        return [
            'customerType' => function ($model) {
                return ArrayHelper::getValue($model, 'custType0');
            },
            'wishlist' => function ($model) {
                return ArrayHelper::getValue($model, 'wishlists');
            },
        ];
    }
}
