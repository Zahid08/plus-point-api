<?php

namespace api\modules\v1\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property string $id
 * @property string $branchId
 * @property string $supplierId
 * @property string $custId
 * @property string $username
 * @property string $auth_key
 * @property string $password
 * @property string $salt
 * @property string $password_reset_token
 * @property string $ipAddress
 * @property string $browser
 * @property string $os
 * @property string $lastlogin
 * @property string $lastlogout
 * @property string $totalLoginTime
 * @property string $status
 *
 * @property Advertisement[] $advertisements
 * @property Attributes[] $attributes
 * @property Attributes[] $attributes0
 * @property AttributesType[] $attributesTypes
 * @property AttributesType[] $attributesTypes0
 * @property Bank[] $banks
 * @property Bank[] $banks0
 * @property BankDeposit[] $bankDeposits
 * @property BankDeposit[] $bankDeposits0
 * @property BankWithdraw[] $bankWithdraws
 * @property BankWithdraw[] $bankWithdraws0
 * @property Branch[] $branches
 * @property Branch[] $branches0
 * @property BranchUsertypeActions[] $branchUsertypeActions
 * @property BranchUsertypeActions[] $branchUsertypeActions0
 * @property Brand[] $brands
 * @property Brand[] $brands0
 * @property CardTypes[] $cardTypes
 * @property CardTypes[] $cardTypes0
 * @property Category[] $categories
 * @property Category[] $categories0
 * @property Company[] $companies
 * @property Company[] $companies0
 * @property ContActions[] $contActions
 * @property ContActions[] $contActions0
 * @property Contents[] $contents
 * @property Contents[] $contents0
 * @property ContentsType[] $contentsTypes
 * @property ContentsType[] $contentsTypes0
 * @property Controllers[] $controllers
 * @property Controllers[] $controllers0
 * @property Coupons[] $coupons
 * @property Coupons[] $coupons0
 * @property CustomScripts[] $customScripts
 * @property Customer[] $customers
 * @property Customer[] $customers0
 * @property CustomerDeposit[] $customerDeposits
 * @property CustomerDeposit[] $customerDeposits0
 * @property CustomerLoyalty[] $customerLoyalties
 * @property CustomerLoyalty[] $customerLoyalties0
 * @property CustomerType[] $customerTypes
 * @property CustomerType[] $customerTypes0
 * @property DamageWastage[] $damageWastages
 * @property Department[] $departments
 * @property Department[] $departments0
 * @property Finance[] $finances
 * @property Finance[] $finances0
 * @property FinanceCategory[] $financeCategories
 * @property FinanceCategory[] $financeCategories0
 * @property GoodReceiveNote[] $goodReceiveNotes
 * @property GoodReceiveNote[] $goodReceiveNotes0
 * @property InvSettings[] $invSettings
 * @property InvSettings[] $invSettings0
 * @property InvSummary[] $invSummaries
 * @property InvSummary[] $invSummaries0
 * @property Inventory[] $inventories
 * @property ItemImages[] $itemImages
 * @property ItemParents[] $itemParents
 * @property Items[] $items
 * @property Items[] $items0
 * @property ItemsPrice[] $itemsPrices
 * @property ItemsPrice[] $itemsPrices0
 * @property PaymentMethod[] $paymentMethods
 * @property PaymentMethod[] $paymentMethods0
 * @property PoDetails[] $poDetails
 * @property PoReturns[] $poReturns
 * @property PofferPackages[] $pofferPackages
 * @property PofferPackages[] $pofferPackages0
 * @property Poffers[] $poffers
 * @property Poffers[] $poffers0
 * @property PurchaseOrder[] $purchaseOrders
 * @property PurchaseOrder[] $purchaseOrders0
 * @property Roles[] $roles
 * @property Roles[] $roles0
 * @property Roles[] $roles1
 * @property SalesDetails[] $salesDetails
 * @property SalesDetails[] $salesDetails0
 * @property SalesInvoice[] $salesInvoices
 * @property SalesInvoice[] $salesInvoices0
 * @property SalesReturn[] $salesReturns
 * @property SalesSpecial[] $salesSpecials
 * @property Seo[] $seos
 * @property ShippingMethod[] $shippingMethods
 * @property ShippingMethod[] $shippingMethods0
 * @property Sliders[] $sliders
 * @property Sliders[] $sliders0
 * @property SocialNetwork[] $socialNetworks
 * @property SocialNetwork[] $socialNetworks0
 * @property Stock[] $stocks
 * @property StockTransfer[] $stockTransfers
 * @property Subdepartment[] $subdepartments
 * @property Subdepartment[] $subdepartments0
 * @property Supplier[] $suppliers
 * @property Supplier[] $suppliers0
 * @property SupplierDeposit[] $supplierDeposits
 * @property SupplierDeposit[] $supplierDeposits0
 * @property SupplierWidraw[] $supplierWidraws
 * @property SupplierWidraw[] $supplierWidraws0
 * @property Tax[] $taxes
 * @property Tax[] $taxes0
 * @property TemporaryWorker[] $temporaryWorkers
 * @property TemporaryWorker[] $temporaryWorkers0
 * @property TemporaryWorkerWidraw[] $temporaryWorkerWidraws
 * @property TemporaryWorkerWidraw[] $temporaryWorkerWidraws0
 * @property TemporaryWorkerWidraw[] $temporaryWorkerWidraws1
 * @property Tracer[] $tracers
 * @property Tracer[] $tracers0
 * @property Tracer[] $tracers1
 * @property UserType[] $userTypes
 * @property UserType[] $userTypes0
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE = 1;

    const STATUS_INACTIVE = 2;

    const STATUS_DELETED = -2;

    const SCENARIO_ACCOUNT = 'account';

    public $currentPassword;

    public $newPassword;

    public $newPasswordConfirm;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branchId', 'username', 'password', 'salt'], 'required'],

            [['currentPassword'], 'required', 'on' => self::SCENARIO_ACCOUNT],
            [['currentPassword'], 'validateCurrentPassword'],

            [['branchId', 'supplierId', 'custId', 'totalLoginTime', 'status'], 'integer'],
            [['username', 'password', 'password_reset_token', 'salt', 'browser', 'os'], 'string', 'max' => 128],
            [['auth_key'], 'string', 'max' => 32],
            [['ipAddress'], 'string', 'max' => 20],
            [['lastlogin', 'lastlogout'], 'string', 'max' => 12],

            [['newPassword', 'password'], 'string', 'min' => 3],
            [['newPassword', 'password'], 'filter', 'filter' => 'trim'],
            [['newPasswordConfirm'], 'compare', 'compareAttribute' => 'newPassword', 'message' => Yii::t('app', 'Passwords do not match')],
        ];
    }


    /**
     * Validate current password (account page)
     */
    public function validateCurrentPassword()
    {
        if (!$this->validatePassword($this->currentPassword)) {
            throw new Exception('Current password incorrect');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'branchId' => Yii::t('app', 'Branch ID'),
            'supplierId' => Yii::t('app', 'Supplier ID'),
            'custId' => Yii::t('app', 'Cust ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'salt' => Yii::t('app', 'Salt'),
            'ipAddress' => Yii::t('app', 'Ip Address'),
            'browser' => Yii::t('app', 'Browser'),
            'os' => Yii::t('app', 'Os'),
            'lastlogin' => Yii::t('app', 'Lastlogin'),
            'lastlogout' => Yii::t('app', 'Lastlogout'),
            'totalLoginTime' => Yii::t('app', 'Total Login Time'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertisements()
    {
        return $this->hasMany(Advertisement::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /*public function getAttributes()
    {
        return $this->hasMany(Attributes::className(), ['crBy' => 'id']);
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
   /* public function getAttributes0()
    {
        return $this->hasMany(Attributes::className(), ['moBy' => 'id']);
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributesTypes()
    {
        return $this->hasMany(AttributesType::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributesTypes0()
    {
        return $this->hasMany(AttributesType::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanks()
    {
        return $this->hasMany(Bank::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanks0()
    {
        return $this->hasMany(Bank::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankDeposits()
    {
        return $this->hasMany(BankDeposit::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankDeposits0()
    {
        return $this->hasMany(BankDeposit::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankWithdraws()
    {
        return $this->hasMany(BankWithdraw::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankWithdraws0()
    {
        return $this->hasMany(BankWithdraw::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranches()
    {
        return $this->hasMany(Branch::className(), ['id' => 'branchId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranches0()
    {
        return $this->hasOne(Branch::className(), ['id' => 'branchId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranchUsertypeActions()
    {
        return $this->hasMany(BranchUsertypeActions::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranchUsertypeActions0()
    {
        return $this->hasMany(BranchUsertypeActions::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrands()
    {
        return $this->hasMany(Brand::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrands0()
    {
        return $this->hasMany(Brand::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardTypes()
    {
        return $this->hasMany(CardTypes::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardTypes0()
    {
        return $this->hasMany(CardTypes::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories0()
    {
        return $this->hasMany(Category::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies0()
    {
        return $this->hasMany(Company::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContActions()
    {
        return $this->hasMany(ContActions::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContActions0()
    {
        return $this->hasMany(ContActions::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContents()
    {
        return $this->hasMany(Contents::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContents0()
    {
        return $this->hasMany(Contents::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentsTypes()
    {
        return $this->hasMany(ContentsType::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentsTypes0()
    {
        return $this->hasMany(ContentsType::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getControllers()
    {
        return $this->hasMany(Controllers::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getControllers0()
    {
        return $this->hasMany(Controllers::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoupons()
    {
        return $this->hasMany(Coupons::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoupons0()
    {
        return $this->hasMany(Coupons::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomScripts()
    {
        return $this->hasMany(CustomScripts::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customer::className(), ['id' => 'custId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers0()
    {
        return $this->hasOne(Customer::className(), ['id' => 'custId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerDeposits()
    {
        return $this->hasMany(CustomerDeposit::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerDeposits0()
    {
        return $this->hasMany(CustomerDeposit::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerLoyalties()
    {
        return $this->hasMany(CustomerLoyalty::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerLoyalties0()
    {
        return $this->hasMany(CustomerLoyalty::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerTypes()
    {
        return $this->hasMany(CustomerType::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerTypes0()
    {
        return $this->hasMany(CustomerType::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDamageWastages()
    {
        return $this->hasMany(DamageWastage::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartments()
    {
        return $this->hasMany(Department::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartments0()
    {
        return $this->hasMany(Department::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinances()
    {
        return $this->hasMany(Finance::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinances0()
    {
        return $this->hasMany(Finance::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCategories()
    {
        return $this->hasMany(FinanceCategory::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCategories0()
    {
        return $this->hasMany(FinanceCategory::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodReceiveNotes()
    {
        return $this->hasMany(GoodReceiveNote::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodReceiveNotes0()
    {
        return $this->hasMany(GoodReceiveNote::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvSettings()
    {
        return $this->hasMany(InvSettings::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvSettings0()
    {
        return $this->hasMany(InvSettings::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvSummaries()
    {
        return $this->hasMany(InvSummary::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvSummaries0()
    {
        return $this->hasMany(InvSummary::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventories()
    {
        return $this->hasMany(Inventory::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemImages()
    {
        return $this->hasMany(ItemImages::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemParents()
    {
        return $this->hasMany(ItemParents::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Items::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems0()
    {
        return $this->hasMany(Items::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemsPrices()
    {
        return $this->hasMany(ItemsPrice::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemsPrices0()
    {
        return $this->hasMany(ItemsPrice::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethods()
    {
        return $this->hasMany(PaymentMethod::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethods0()
    {
        return $this->hasMany(PaymentMethod::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoDetails()
    {
        return $this->hasMany(PoDetails::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoReturns()
    {
        return $this->hasMany(PoReturns::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPofferPackages()
    {
        return $this->hasMany(PofferPackages::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPofferPackages0()
    {
        return $this->hasMany(PofferPackages::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoffers()
    {
        return $this->hasMany(Poffers::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoffers0()
    {
        return $this->hasMany(Poffers::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrders()
    {
        return $this->hasMany(PurchaseOrder::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrders0()
    {
        return $this->hasMany(PurchaseOrder::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasMany(Roles::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles0()
    {
        return $this->hasOne(Roles::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles1()
    {
        return $this->hasMany(Roles::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesDetails()
    {
        return $this->hasMany(SalesDetails::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesDetails0()
    {
        return $this->hasMany(SalesDetails::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesInvoices()
    {
        return $this->hasMany(SalesInvoice::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesInvoices0()
    {
        return $this->hasMany(SalesInvoice::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesReturns()
    {
        return $this->hasMany(SalesReturn::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesSpecials()
    {
        return $this->hasMany(SalesSpecial::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeos()
    {
        return $this->hasMany(Seo::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShippingMethods()
    {
        return $this->hasMany(ShippingMethod::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShippingMethods0()
    {
        return $this->hasMany(ShippingMethod::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSliders()
    {
        return $this->hasMany(Sliders::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSliders0()
    {
        return $this->hasMany(Sliders::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialNetworks()
    {
        return $this->hasMany(SocialNetwork::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialNetworks0()
    {
        return $this->hasMany(SocialNetwork::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStocks()
    {
        return $this->hasMany(Stock::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockTransfers()
    {
        return $this->hasMany(StockTransfer::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubdepartments()
    {
        return $this->hasMany(Subdepartment::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubdepartments0()
    {
        return $this->hasMany(Subdepartment::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuppliers()
    {
        return $this->hasMany(Supplier::className(), ['id' => 'supplierId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuppliers0()
    {
        return $this->hasOne(Supplier::className(), ['id' => 'supplierId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplierDeposits()
    {
        return $this->hasMany(SupplierDeposit::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplierDeposits0()
    {
        return $this->hasMany(SupplierDeposit::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplierWidraws()
    {
        return $this->hasMany(SupplierWidraw::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplierWidraws0()
    {
        return $this->hasMany(SupplierWidraw::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxes()
    {
        return $this->hasMany(Tax::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxes0()
    {
        return $this->hasMany(Tax::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemporaryWorkers()
    {
        return $this->hasMany(TemporaryWorker::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemporaryWorkers0()
    {
        return $this->hasMany(TemporaryWorker::className(), ['wkId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemporaryWorkerWidraws()
    {
        return $this->hasMany(TemporaryWorkerWidraw::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemporaryWorkerWidraws0()
    {
        return $this->hasMany(TemporaryWorkerWidraw::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemporaryWorkerWidraws1()
    {
        return $this->hasMany(TemporaryWorkerWidraw::className(), ['wkId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTracers()
    {
        return $this->hasMany(Tracer::className(), ['cashierId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTracers0()
    {
        return $this->hasMany(Tracer::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTracers1()
    {
        return $this->hasMany(Tracer::className(), ['moBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTypes()
    {
        return $this->hasMany(UserType::className(), ['crBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTypes0()
    {
        return $this->hasMany(UserType::className(), ['moBy' => 'id']);
    }

    /** Get All Active User Drop Down List*/
    public static function userDropDownList($status = 1)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findUserByCustomerId($custId)
    {
        return static::findOne(['custId' => $custId, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->hashPassword($password,$this->salt) === $this->password;
    }

    /**
     * Generates the password hash.
     * @param string password
     * @param string salt
     * @return string hash
     */
    public function hashPassword($password,$salt)
    {
        return md5($salt.$password);
    }

    /**
     * Generates a salt that can be used to generate a password hash.
     * @return string the salt
     */
    public function generateSalt()
    {
        return uniqid('',true);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Update login info (ip and time)
     * @return bool
     */
    public function updateLoginMeta()
    {
        $this->logged_in_ip = Yii::$app->request->userIP;
        $this->logged_in_at = gmdate("Y-m-d H:i:s");
        return $this->save(false, ["logged_in_ip", "logged_in_at"]);
    }

    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        return $user->save() ? $user : null;
    }

    public static function getLoggedInUserAdditionalRoles()
    {
        $roleIds = [];

        $roles = isset(Yii::$app->user->identity->additional_roles) ? Yii::$app->user->identity->additional_roles : null;

        if($roles){
            $roleIds = array_map('intval', explode(',', $roles));
        }

        array_push($roleIds, CommonHelper::ROLE_GUEST_PK);

        return $roleIds;
    }

    public static function getLoggedInUserSubtractionRoles()
    {
        $roles = isset(Yii::$app->user->identity->subtraction_roles) ? Yii::$app->user->identity->subtraction_roles : null;

        if($roles){
            $roleIds = array_map('intval', explode(',', $roles));
            return $roleIds;
        }

        return null;
    }

    public static function setForcePasswordChange($userId)
    {
        if(empty($userId)){
            return false;
        }

        $user = User::find()
            ->where(['id' => $userId])
            ->one();

        if($user){
            $user->force_password_change = 1;
            $user->save(false);

            return true;
        }
    }

    /*public function fields()
    {
        return [
            'id',
            'branchId',
            'branchName' => function ($model) {
                return ArrayHelper::getValue($model, 'branches0.name');
                //return $model->branches0->name;
              //  return isset($model->branches0->name) ? $model->branches0->name : '';
            },
            'supplierId',
            'supplierInfo' => function ($model) {
                return $model->suppliers;
            },
            'custId',
            'customerInfo' => function ($model) {
                return $model->customers;
            },
            'username',
            'status',
            'userRole' => function ($model) {
                return $model->roles;
            },
        ];
    }*/

    public function fields()
    {
        $fields = parent::fields();

        unset($fields['password'], $fields['salt'], $fields['password_reset_token']);

        $additionalFields = [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];

        $result = ArrayHelper::merge($fields, $additionalFields);

        return $result;
    }

    public function extraFields()
    {
        return [
            'branch' => function ($model) {
                return ArrayHelper::getValue($model, 'branches0');
            },
            'supplier' => function ($model) {
                return ArrayHelper::getValue($model, 'suppliers0');
            },
            'customer' => function ($model) {
                return ArrayHelper::getValue($model, 'customers0');
            },
            'role' => function ($model) {
                return ArrayHelper::getValue($model, 'roles0');
            },
        ];
    }
}
