<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%po_details}}".
 *
 * @property string $id
 * @property string $poId
 * @property string $itemId
 * @property double $qty
 * @property double $costPrice
 * @property string $currentStock
 * @property string $soldQty
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * @property User $crBy0
 * @property Items $item
 * @property PurchaseOrder $po
 */
class PoDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%po_details}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['poId', 'itemId', 'currentStock', 'soldQty', 'crBy', 'moBy', 'status'], 'integer'],
            [['qty', 'costPrice'], 'number'],
            [['crAt', 'moAt'], 'safe'],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['itemId'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['itemId' => 'id']],
            [['poId'], 'exist', 'skipOnError' => true, 'targetClass' => PurchaseOrder::className(), 'targetAttribute' => ['poId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'poId' => Yii::t('app', 'Po ID'),
            'itemId' => Yii::t('app', 'Item ID'),
            'qty' => Yii::t('app', 'Qty'),
            'costPrice' => Yii::t('app', 'Cost Price'),
            'currentStock' => Yii::t('app', 'Current Stock'),
            'soldQty' => Yii::t('app', 'Sold Qty'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::className(), ['id' => 'itemId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPo()
    {
        return $this->hasOne(PurchaseOrder::className(), ['id' => 'poId']);
    }

    /** Get All Active PoDetails Drop Down List*/
    public static function poDetailsDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }

    public function fields()
    {
        return [
            'id',
            'poId',
            'itemId',
            'qty',
            'costPrice',
            'currentStock',
            'soldQty',
            'crAt',
            'crBy',
            'moAt',
            'moBy',
            'status',
        ];
    }

    public function extraFields()
    {
        return [
            'itemDetails' => function ($model) {
                return $model->item;
            },
        ];
    }
}
