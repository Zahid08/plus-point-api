<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%controllers}}".
 *
 * @property string $id
 * @property string $module
 * @property string $name
 * @property string $description
 * @property string $status
 * @property string $crBy
 * @property string $crAt
 * @property string $moBy
 * @property string $moAt
 *
 * @property ContActions[] $contActions
 * @property User $crBy0
 * @property User $moBy0
 */
class Controllers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%controllers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['status', 'crBy', 'moBy'], 'integer'],
            [['crAt', 'moAt'], 'safe'],
            [['module', 'name'], 'string', 'max' => 50],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'module' => Yii::t('app', 'Module'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'status' => Yii::t('app', 'Status'),
            'crBy' => Yii::t('app', 'Cr By'),
            'crAt' => Yii::t('app', 'Cr At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'moAt' => Yii::t('app', 'Mo At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContActions()
    {
        return $this->hasMany(ContActions::className(), ['contId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /** Get All Active Controllers Drop Down List*/
    public static function controllersDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }
}
