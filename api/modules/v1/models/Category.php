<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property string $id
 * @property string $subdeptId
 * @property string $name
 * @property string $name_bd
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $rank
 *
 * @property User $crBy0
 * @property User $moBy0
 * @property Subdepartment $subdept
 * @property Items[] $items
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subdeptId', 'status', 'crBy', 'moBy', 'rank'], 'integer'],
            [['crAt', 'moAt'], 'safe'],
            [['name', 'name_bd'], 'string', 'max' => 50],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['moBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['moBy' => 'id']],
            [['subdeptId'], 'exist', 'skipOnError' => true, 'targetClass' => Subdepartment::className(), 'targetAttribute' => ['subdeptId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'subdeptId' => Yii::t('app', 'Subdept ID'),
            'name' => Yii::t('app', 'Name'),
            'name_bd' => Yii::t('app', 'Name (বাংলা)'),
            'status' => Yii::t('app', 'Status'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'rank' => Yii::t('app', 'Rank'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'moBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubdept()
    {
        return $this->hasOne(Subdepartment::className(), ['id' => 'subdeptId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Items::className(), ['catId' => 'id']);
    }

    /** Get All Active Category Drop Down List*/
    public static function categoryDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }

    public function fields()
    {
        return [
            'id',
            'departmentId' => function ($model) {
                return ArrayHelper::getValue($model, 'subdept.deptId');
            },
            'departmentName' => function ($model) {
                $deptId = ArrayHelper::getValue($model, 'subdept.deptId');
                $departmentInfo = Department::getDepartmentInfoById($deptId);
                return ArrayHelper::getValue($departmentInfo, 'name');
            },
            'departmentName_bd' => function ($model) {
                $deptId = ArrayHelper::getValue($model, 'subdept.deptId');
                $departmentInfo = Department::getDepartmentInfoById($deptId);
                return ArrayHelper::getValue($departmentInfo, 'name_bd');
            },
            'subdeptId',
            'subDepartmentName' => function ($model) {
                return ArrayHelper::getValue($model, 'subdept.name');
            },
            'subDepartmentName_bd' => function ($model) {
                return ArrayHelper::getValue($model, 'subdept.name_bd');
            },
            'name',
            'name_bd',
            'image' => function ($model) {

                return "http://pluspointretail.unlockretail.com/media/catalog/category/".$model->image;
            },
            'status',
            'crAt',
            'crBy',
            'crByName' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0.username');
            },
            'moAt',
            'moBy',
            'moByName' => function ($model) {
                return ArrayHelper::getValue($model, 'moBy0.username');
            },
            'rank',
        ];
    }
}
