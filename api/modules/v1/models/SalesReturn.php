<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%sales_return}}".
 *
 * @property string $id
 * @property string $salesId
 * @property string $branchId
 * @property string $itemId
 * @property double $qty
 * @property string $returnDate
 * @property double $totalPrice
 * @property double $totalTax
 * @property double $totalDiscount
 * @property double $totalSpDiscount
 * @property double $totalInsDiscount
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * @property Branch $branch
 * @property User $crBy0
 * @property Items $item
 * @property SalesInvoice $sales
 */
class SalesReturn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sales_return}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['salesId', 'branchId', 'itemId', 'crBy', 'moBy', 'status'], 'integer'],
            [['qty', 'totalPrice', 'totalTax', 'totalDiscount', 'totalSpDiscount', 'totalInsDiscount'], 'number'],
            [['returnDate', 'crAt', 'moAt'], 'safe'],
            [['branchId'], 'exist', 'skipOnError' => true, 'targetClass' => Branch::className(), 'targetAttribute' => ['branchId' => 'id']],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
            [['itemId'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['itemId' => 'id']],
            [['salesId'], 'exist', 'skipOnError' => true, 'targetClass' => SalesInvoice::className(), 'targetAttribute' => ['salesId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'salesId' => Yii::t('app', 'Sales ID'),
            'branchId' => Yii::t('app', 'Branch ID'),
            'itemId' => Yii::t('app', 'Item ID'),
            'qty' => Yii::t('app', 'Qty'),
            'returnDate' => Yii::t('app', 'Return Date'),
            'totalPrice' => Yii::t('app', 'Total Price'),
            'totalTax' => Yii::t('app', 'Total Tax'),
            'totalDiscount' => Yii::t('app', 'Total Discount'),
            'totalSpDiscount' => Yii::t('app', 'Total Sp Discount'),
            'totalInsDiscount' => Yii::t('app', 'Total Ins Discount'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(Branch::className(), ['id' => 'branchId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::className(), ['id' => 'itemId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSales()
    {
        return $this->hasOne(SalesInvoice::className(), ['id' => 'salesId']);
    }

    /** Get All Active SalesReturn Drop Down List*/
    public static function salesReturnDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }
}
