<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%custom_scripts}}".
 *
 * @property string $id
 * @property string $title
 * @property string $script
 * @property string $position
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property integer $status
 *
 * @property User $crBy0
 */
class CustomScripts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%custom_scripts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'script', 'position'], 'string'],
            [['crAt', 'moAt'], 'safe'],
            [['crBy', 'status'], 'required'],
            [['crBy', 'moBy', 'status'], 'integer'],
            [['crBy'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['crBy' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'script' => Yii::t('app', 'Script'),
            'position' => Yii::t('app', 'Position'),
            'crAt' => Yii::t('app', 'Cr At'),
            'crBy' => Yii::t('app', 'Cr By'),
            'moAt' => Yii::t('app', 'Mo At'),
            'moBy' => Yii::t('app', 'Mo By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'crBy']);
    }

    /** Get All Active CustomScripts Drop Down List*/
    public static function customScriptsDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->crBy = CommonHelper::getLoggedInUserId();
                $this->crAt = date('Y-m-d h:i:s');
            }
            else
            {
                $this->moBy = CommonHelper::getLoggedInUserId();
                $this->moAt = date('Y-m-d h:i:s');
            }
            return true;
        }
        else return false;
    }
}
