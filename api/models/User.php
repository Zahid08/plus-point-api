<?php

namespace api\models;

use api\helpers\CommonHelper;
use Yii;

class User extends \api\modules\v1\models\User implements \yii\filters\RateLimitInterface
{
    private $rateLimitEnable = false;

    private $rateLimit = 100;

    private $rateLimitWindow = 60;

    public function getRateLimit($request, $action) {
        return [$this->rateLimit, $this->rateLimitWindow]; // 100($this->rateLimit) requests per 60($this->rateLimitWindow) second
    }

    public function loadAllowance($request, $action)
    {
        if($this->rateLimitEnable == false){
            return [$this->rateLimit, time()];
        }

        $hash = Yii::$app->user->id . $request->getUrl() . $action->id;
        $allowance = Yii::$app->cache->get($hash);

        if ($allowance === false) {
            return [$this->rateLimit, time()];
        }
        else{
            return $allowance;
        }

    }

    public function saveAllowance($request, $action, $allowance, $timestamp)
    {
        $hash = Yii::$app->user->id . $request->getUrl() . $action->id;

        $allowance = [$allowance, $timestamp];

        Yii::$app->cache->set($hash, $allowance, $this->rateLimitWindow);
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return static
     *
     * HttpBasicAuth:
     * Authorization: Basic {base64_encode(auth_key:)}
     *
     * QueryParamAuth:
     * ?access-token=auth_key
     *
     * HttpBearerAuth:
     * Authorization: Bearer auth_key
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

}