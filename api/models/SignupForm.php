<?php
namespace api\models;

use api\helpers\CommonHelper;
use Yii;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $password;

    public $supplierId;
    public $custId;
    public $branchId;

    /** @var User */
    public $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\api\modules\v1\models\User', 'message' => Yii::t('app', 'This username has already been taken.')],
            ['username', 'string', 'length' => [3, 25]],
            ['username', 'match', 'pattern' => '/^[A-Za-z0-9_-]{3,25}$/', 'message' => Yii::t('app', 'Your username can only contain alphanumeric characters, underscores and dashes.')],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            [['supplierId', 'custId', 'branchId'], 'safe'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return boolean the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {

            $user = new User();
            $user->username = strtolower($this->username);

            $salt = $user->generateSalt();
            $user->password = $user->hashPassword($this->password, $salt);
            $user->salt = $salt;

            $user->branchId = $this->branchId;
            $user->supplierId = $this->supplierId;
            $user->custId = $this->custId;
            $user->totalLoginTime = 0;

            $user->generateAuthKey();
            $user->ipAddress = Yii::$app->request->userIP;
            $user->status = CommonHelper::STATUS_ACTIVE;

            if($user->save(false)) {
                $this->_user = $user;
                return true;
            }

            return false;
        }
        return false;
    }


    /**
     * Return User object
     *
     * @return User
     */
    public function getUser(){
        return $this->_user;
    }


    public function sendConfirmationEmail(){

        $confirmURL = \Yii::$app->params['frontendURL'].'#/confirm?id='.$this->_user->id.'&auth_key='.$this->_user->auth_key;

        $email = \Yii::$app->mailer
            ->compose(
                ['html' =>  'signup-confirmation-html'],
                [
                    'appName'       =>  \Yii::$app->name,
                    'confirmURL'    =>  $confirmURL,
                ]
            )
            ->setTo($this->email)
            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name])
            ->setSubject('Signup confirmation')
            ->send();

        return $email;
    }
}