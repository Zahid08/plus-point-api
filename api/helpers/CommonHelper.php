<?php
namespace api\helpers;

use Yii;
use yii\helpers\ArrayHelper;

class CommonHelper {

    const GRID_FILTER_PROMPT = 'All';
    const GRID_PER_PAGE = 50;
    const GRID_PER_PAGE_VALUES = '5,10,20,30,40,50,100,200,500';
    const INTEGRITY_CONSTRAINT_VIOLATION = 'Integrity constraint violation';

    // Status
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_DELETED = -2;

    const STATUS_PAID = 1;
    const STATUS_APPROVED = 2;
    const STATUS_CANCEL = 3;
    const STATUS_PENDING = 4;
    const STATUS_VERIFIED = 5;

    // Boolean
    const YES = 1;
    const NO = 0;

    // Role Type
    const  ROLE_TYPE_MAIN = 1;
    const  ROLE_TYPE_ADDITIONAL = 2;
    const  ROLE_TYPE_SUBTRACTION = 3;

    // Role
    const  ROLE_SUPER_ADMIN_PK = 1;
    const  ROLE_ADMIN_PK = 2;
    const  ROLE_REGISTERED_PK = 3;
    const  ROLE_GUEST_PK = 4;

    const  ROLE_SUPER_ADMIN = 'superAdmin';
    const  ROLE_ADMIN = 'admin';
    const  ROLE_REGISTERED = 'registered';
    const  ROLE_GUEST = 'guest';

    // User Group
    const USER_GROUP_ADMIN = 'admin';
    const USER_GROUP_REGISTERED = 'registered';
    const USER_GROUP_GUEST = 'guest';

    const USER_TYPE_CUSTOMER = 3;
    const USER_TYPE_SUPPLIER = 4;

    const CUSTOMER_TYPE_GENERAL = 1;

    const CREDIT_SUPP = 'credit';
    const CONSIG_SUPP = 'consignment';

    public static $reservedParams = ['page', 'pageSize', 'sort', 'fields'];

    // Status Drop Down List
    public static function statusDropDownList($id = null)
    {
        $data = [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    // Status Drop Down List
    public static function roleTypeDropDownList($id = null)
    {
        $data = [
            self::ROLE_TYPE_MAIN => 'Main Rule',
            self::ROLE_TYPE_ADDITIONAL => 'Additional Rule',
            self::ROLE_TYPE_SUBTRACTION => 'Subtraction Rule',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    public static function getPaymentStatusList($id = null)
    {
        $data = [
            self::STATUS_PAID => 'Paid',
            self::STATUS_APPROVED => 'Approved',
            self::STATUS_CANCEL => 'Canceled',
            self::STATUS_PENDING => 'Pending',
            self::STATUS_VERIFIED => 'Verified',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    // Boolean  Drop Down List
    public static function booleanDropDownList($id=null)
    {
        $data = [
            self::YES => 'Yes',
            self::NO => 'No',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    public static function getUserGroupList($id = null)
    {
        $data = [
            self::USER_GROUP_ADMIN => 'Admin',
            self::USER_GROUP_REGISTERED => 'Registered',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    public static function getLoggedInUserId()
    {
        return isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : null;
    }

    public static function getLoggedInUserName()
    {
        return isset(Yii::$app->user->identity->username) ? Yii::$app->user->identity->username : null;
    }

    public static function getLoggedInUserGroup()
    {
        return isset(Yii::$app->user->identity->user_group) ? Yii::$app->user->identity->user_group : null;
    }

    public static function getLoggedInUserRoleId()
    {
        return  isset(Yii::$app->user->identity->role_id) ? Yii::$app->user->identity->role_id : null;
    }

    public static function getLoggedInUserRoleName()
    {
        return  isset(Yii::$app->user->identity->role->name) ? Yii::$app->user->identity->role->name : null;
    }

    public static function isChangedAttribute($model, $compareAttribute){
        $isAttributeChange = false;

        if($compareAttribute){
            foreach ($compareAttribute as $name) {
                if (isset($model->attributes[$name], $model->oldAttributes[$name])) {
                    //Date and DateTime
                    if (in_array($model->getTableSchema()->columns[$name]->type, ['date', 'datetime'])) {
                        if (CommonHelper::changeDateFormat($model->attributes[$name], 'Y-m-d') != CommonHelper::changeDateFormat($model->oldAttributes[$name], 'Y-m-d')) {
                            $isAttributeChange = true;
                        }
                    }
                    else {
                        if ($model->attributes[$name] != $model->oldAttributes[$name]) {
                            $isAttributeChange = true;
                        }
                    }
                }
            }
        }

        return $isAttributeChange;
    }

    public static function getCurrentDate()
    {
        $current_date = date('d-m-Y');
        return $current_date;
    }

    public static function dateTimeDifference($date)
    {
        $currentDate=strtotime(date('d M Y G.i.s'));
        $reviewDate = strtotime($date);
        $all = round(($currentDate - $reviewDate) / 60);
        $d = floor ($all / 1440);
        $h = floor (($all - $d * 1440) / 60);
        $m = $all - ($d  *1440) - ($h* 60);//Since you need just hours and mins
        return array(
            'd'=>$d,
            'h'=>$h,
            'm'=>$m
        );
    }

    public function dateDifference($startDate, $endDate)
    {
        $dateOne = date_create($startDate);
        $dateTwo = date_create($endDate);
        return $dateDifference = date_diff($dateTwo, $dateOne);
    }

    public static function getCurrentMonth()
    {
        $current_month = date('m');
        return (int)$current_month;
    }

    public static function getNextMonth($month = null)
    {
        if($month){
            $date = date('Y') . '-' . $month . '-' . date('d');
            $next_month = date('m', strtotime('+1 month', strtotime($date)));
        }
        else{
            $next_month = date('m', strtotime('+1 month'));
        }

        return (int)$next_month;
    }

    /// Month List
    public static function getMonthList($id=null)
    {
        $data = [
            1 =>  'January',
            2 =>  'February',
            3 =>  'March',
            4 =>  'April',
            5 =>  'May',
            6 =>  'June ',
            7 =>  'July',
            8 =>  'August',
            9 =>  'September',
            10 => 'October',
            11 => 'November',
            12 => 'December',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    public static function getMonthListFromDate($startDate, $endDate){
        $months = [];

        $begin = new \DateTime($startDate);
        $end = new \DateTime($endDate);

        while ($begin <= $end) {
            $months[] = (int) $begin->format('m');
            $begin->modify('first day of next month');
        }

        return $months;
    }

    public static function getCurrentFinancialYear()
    {
        if (date('m') > 6) {
            $year = date('Y')."-".(date('Y') +1);
        }
        else {
            $year = (date('Y')-1)."-".date('Y');
        }

        return $year;
    }

    public static function getPreviousFinancialYear($financialYear) {
        $array = explode('-', $financialYear);
        $firstYear = $array[0]-1;
        $lastYear = $array[1]-1;
        return $previousFinancialYear = $firstYear.'-'.$lastYear;
    }

    public static function getYearFromFinancialYearAndMonth($financialYear, $month)
    {
        $yearArray = explode('-', $financialYear);
        $startYear = $yearArray[0];
        $endYear = $yearArray[1];

        if ($month > 6) {
            $year = $startYear;
        }
        else {
            $year = $endYear;
        }

        return $year;
    }

    public static function getNextFinancialYear($financialYear) {
        $array = explode('-', $financialYear);
        $firstYear = $array[0]+1;
        $lastYear = $array[1]+1;
        return $previousFinancialYear = $firstYear.'-'.$lastYear;
    }

    public static function getFinancialYearStartDate($financialYear)
    {
        $yearArray = explode('-',$financialYear);
        $startYear = $yearArray[0];

        // Start Date
        $startDate = $startYear . '-07-01';

        return $startDate;
    }

    public static function getFinancialYearEndDate($financialYear)
    {
        $yearArray = explode('-',$financialYear);
        $endYear = $yearArray[1];

        // Start Date
        $endDate = $endYear . '-06-30';

        return $endDate;
    }

    public static function percentCalculation($smallAmount, $bigAmount)
    {
        if(empty($smallAmount) || empty($bigAmount)){
            return 0;
        }

        if($bigAmount > $smallAmount){
            $diffPercent = ($smallAmount / $bigAmount) * 100;
        }
        else{
            $diffPercent = ($bigAmount / $smallAmount) * 100;
        }

        return number_format($diffPercent, 2);
    }

    public static function changeDateFormat($date, $format = 'Y-m-d')
    {
        if (empty($date) || $date === null) {
            return null;
        }

        $date = date($format, strtotime($date));
        return $date;
    }

    // number in words
    public static function amountToWords($number)
    {
        $hyphen      = ' ';
        $conjunction = ' '; // and
        $separator   = ' '; // ,
        $negative    = 'negative ';
        $decimal     = ' and paisa '; // point
        $dictionary  = array(
            0                   => 'zero',
            1                   => 'one',
            2                   => 'two',
            3                   => 'three',
            4                   => 'four',
            5                   => 'five',
            6                   => 'six',
            7                   => 'seven',
            8                   => 'eight',
            9                   => 'nine',
            10                  => 'ten',
            11                  => 'eleven',
            12                  => 'twelve',
            13                  => 'thirteen',
            14                  => 'fourteen',
            15                  => 'fifteen',
            16                  => 'sixteen',
            17                  => 'seventeen',
            18                  => 'eighteen',
            19                  => 'nineteen',
            20                  => 'twenty',
            30                  => 'thirty',
            40                  => 'fourty',
            50                  => 'fifty',
            60                  => 'sixty',
            70                  => 'seventy',
            80                  => 'eighty',
            90                  => 'ninety',
            100                 => 'hundred',
            1000                => 'thousand',
            100000              => 'lac',
            10000000            => 'crore',
            1000000000000       => 'trillion',
            1000000000000000    => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );
        if (!is_numeric($number)) {
            return false;
        }
        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'amountToWords only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }
        if ($number < 0) {
            return $negative.self::amountToWords(abs($number));
        }
        $string = $fraction = null;
        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }
        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction.self::amountToWords($remainder);
                }
                break;

            case $number < 100000:
                $thousands   = ((int) ($number / 1000));
                $remainder = $number % 1000;

                $thousands = self::amountToWords($thousands);

                $string .= $thousands . ' ' . $dictionary[1000];
                if ($remainder) {
                    $string .= $separator . self::amountToWords($remainder);
                }
                break;

            case $number < 10000000:
                $lakhs   = ((int) ($number / 100000));
                $remainder = $number % 100000;

                $lakhs = self::amountToWords($lakhs);

                $string = $lakhs . ' ' . $dictionary[100000];
                if ($remainder) {
                    $string .= $separator . self::amountToWords($remainder);
                }
                break;

            case $number < 1000000000:
                $crores   = ((int) ($number / 10000000));
                $remainder = $number % 10000000;

                $crores = self::amountToWords($crores);

                $string = $crores . ' ' . $dictionary[10000000];
                if ($remainder) {
                    $string .= $separator . self::amountToWords($remainder);
                }
                break;

            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = self::amountToWords($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= self::amountToWords($remainder);
                }
                break;
        }
        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }
        return $string;
    }

    /*
     * Array values into ranges
     * Sample Input: Array(1,2,3,4,5,6,10,11,12,13,20,24)
     * Output: Array([0] => 1-6, [1] => 10-13, [2] => 20, [3] => 24)
     * */
    function getRangeString($aNumbers) {
        $aNumbers = array_unique( $aNumbers );
        sort( $aNumbers );
        $aGroups = array();
        for( $i = 0; $i < count( $aNumbers ); $i++ ) {
            if( $i > 0 && ( $aNumbers[$i-1] == $aNumbers[$i] - 1 ))
                array_push( $aGroups[count($aGroups)-1], $aNumbers[$i] );
            else
                array_push( $aGroups, array( $aNumbers[$i] ));
        }
        $aRanges = array();
        foreach( $aGroups as $aGroup ) {
            if( count( $aGroup ) == 1 )
                $aRanges[] = $aGroup[0];
            else
                $aRanges[] = $aGroup[0] . '-' . $aGroup[count($aGroup)-1];
        }
        return implode( ',', $aRanges );
    }

    function addCurrencySymbol($amount, $currency) {
        if($currency == 'USD'){
            return '$'. $amount;
        }
        else if($currency == 'USD'){
            return '€'. $amount;
        }
        else if($currency == 'GBP'){
            return '£'. $amount;
        }
        else if($currency == 'BDT'){
            return 'Tk'. $amount;
        }

        return $amount;
    }

    // Report As On Date
    public static function reportAsOnDateTime($endDate = null, $startDate = null)
    {

        $startDate = !empty($startDate) ? CommonHelper::changeDateFormat($startDate, 'd M Y') : null;
        $endDate = !empty($endDate) ? CommonHelper::changeDateFormat($endDate, 'd M Y') : date('d-M-Y');

        $asOnDate = $endDate;

        if(!empty($startDate) && !empty($endDate)){
            $asOnDate = $startDate . ' TO ' . $endDate;
        }

        $asOnDate = '(As On ' . $asOnDate . ' AT ' . date('h:i') . ')';

        return $asOnDate;
    }

}
